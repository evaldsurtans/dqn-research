import subprocess
import traceback
import logging
import os
import re
import sys
import time
import param_utils
import copy
import json
import numpy as np

path_input = sys.argv[1]

lines = open(path_input).readlines()
rows = []
rows_numeric = []
header = None

for line in lines:
    row = line.strip().split(';')

    # last empty element
    if len(row[-1].strip()) == 0:
        row = row[:-1]

    if header is None:
        header = row
    else:
        if len(row) != len(header):
            continue
        formated = []
        formated_numeric = []
        for each in row:
            if re.match('^[0-9E\+\-\.]+$', each):
                formated.append(float(each))
                formated_numeric.append(float(each))
            else:
                formated.append(each)
                formated_numeric.append(0)
        rows.append(formated)
        rows_numeric.append(formated_numeric)

complete = []
path_base = os.getcwd() + '/'
with open(path_base + 'results/complete.json', 'r') as outfile:
    complete = json.load(outfile)

for idx, row in enumerate(rows):
    row_numeric = rows_numeric[idx]

    for fix_element in complete:
        is_match = True
        for fix_key in fix_element:
            if fix_key != 'comment':
                if row[header.index(fix_key)] != fix_element[fix_key] and row_numeric[header.index(fix_key)] != fix_element[fix_key]:
                    is_match = False
                    break
        if is_match:
            row[header.index('comment')] = fix_element['comment']

rows.insert(0, header)
rows = [reduce(lambda a, b: str(a) + ';' + str(b), x) + "\n" for x in rows]
open(path_input, 'w').writelines(rows)