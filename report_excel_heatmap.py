import numpy as np
import re
import sys
import copy
import re
import colorlover as cl #sudo pip install colorlover

import openpyxl
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Color, Fill, fills, Border, Side, Font
from openpyxl.cell import Cell

wb = Workbook()


path_input = sys.argv[1] # CSV of combined
path_output = sys.argv[2] # XLSX output

param_y_axis = 'avg(eval_avg)'
param_x_axis = 'lr'
param_relative = False

data = []
data_groups = []
x_axis = []
titles = []

lines = open(path_input).readlines()
header = None

for line in lines:
    row = line.strip().split(';')

    if header is None:
        header = row
    else:
        title = row[header.index('comment')].strip()
        value_y = float(row[header.index(param_y_axis)])
        value_x = float(row[header.index(param_x_axis)])


        data_it = [it for it in data if it['title'] == title]
        if not len(data_it):
            data_it = {
                'title': title,
                'runs': []
            }
            data.append(data_it)
        else:
            data_it = data_it[0]

        data_it['runs'].append({
            'x': value_x,
            'y': value_y
        })

        if not value_x in x_axis:
            x_axis.append(value_x)

        tmp = title.split(':')[0] + ':'
        if not tmp in titles:
            titles.append(tmp)

for title in titles:
    list = [it for it in data if it['title'].startswith(title)]
    list = sorted(list, key=lambda it: it['title'])

    try:
        all_runs = reduce(lambda it2, it3: it2 + it3, [it['runs'] for it in list])

        data_groups.append(
            {
                'title': title,
                'list': list,
                'all_runs': all_runs
            }
        )
    except:
        print 'error data_groups.append {}'.format(title)
        exit()

def set_cell_border(cell):
    side = Side(border_style='thin', color="FF000000")
    border = Border(
        left=side,
        right=side,
        top=side,
        bottom=side
    )
    cell.border = border

def set_border(ws, it_rows):
    rows = []
    for row in it_rows:
        rows.append(row)

    side = Side(border_style='medium', color="FF000000")
    max_y = len(rows) - 1  # index of the last row
    for pos_y, cells in enumerate(rows):
        max_x = len(cells) - 1  # index of the last cell
        for pos_x, cell in enumerate(cells):
            border = Border(
                left=cell.border.left,
                right=cell.border.right,
                top=cell.border.top,
                bottom=cell.border.bottom
            )
            if pos_x == 0:
                border.left = side
            if pos_x == max_x:
                border.right = side
            if pos_y == 0:
                border.top = side
            if pos_y == max_y:
                border.bottom = side

            # set new border only if it's one of the edge cells
            if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
                cell.border = border

ws = wb.active

col = 1
row = 1

colors_max = 100
colors = cl.to_rgb( cl.interp(cl.scales['3']['seq']['Greens'], colors_max) )

font = Font(bold=True)
cell = ws.cell(column=col, row=row, value='Hyper-parameter')
set_cell_border(cell)
cell.font = font
x_axis.sort()
for value_x in x_axis:
    col += 1
    cell = ws.cell(column=col, row=row, value='lr={}'.format(value_x))
    set_cell_border(cell)
    cell.font = font

for data_group_it in data_groups:
    value_y_max = max(data_group_it['all_runs'], key=lambda it: it['y'])['y']
    value_y_min = min(data_group_it['all_runs'], key=lambda it: it['y'])['y']
    value_y_delta = value_y_max - value_y_min

    row_start = row + 1

    for data_it in data_group_it['list']:
        col = 1
        row += 1

        cell = ws.cell(column=col, row=row, value=data_it['title'])
        set_cell_border(cell)
        for value_x in x_axis:
            col += 1
            value_y = [it['y'] for it in data_it['runs'] if it['x'] == value_x]
            if len(value_y):
                value_y = value_y[0]
                cell = ws.cell(column=col, row=row, value=value_y)
                set_cell_border(cell)

                color_idx = int(((value_y - value_y_min) / value_y_delta) * (colors_max - 1))
                color = colors[color_idx]
                color = [int(it) for it in re.findall('[0-9]+', color)]

                cell.fill = PatternFill(start_color=Color(rgb="FF{:02x}{:02x}{:02x}".format(color[0],color[1],color[2]).upper()), fill_type='solid', patternType="solid")


    set_border(ws, ws.iter_rows(min_row=row_start, max_row=row))

for col in ws.columns:
     max_length = 0
     column = col[0].column # Get the column name
     for cell in col:
         try: # Necessary to avoid error on empty cells
             if len(str(cell.value)) > max_length:
                 max_length = len(cell.value)
         except:
             pass
     adjusted_width = (max_length + 2) * 1.2
     ws.column_dimensions[column].width = adjusted_width

wb.save(path_output)




