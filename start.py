import subprocess
import traceback
import os
import re
import sys
import time
import param_utils
import argparse
import re

'''
Script for qsub param validation

python start.py ./path/
^ for directory

python start.py ./path/params.csv --hpc 1
^ for qsub

python start.py ./path/params.csv --hpc 10
^ for qsub repeated 10

python start.py ./path/params.csv --short
^ for short test

python start.py ./path/params.csv --play 
^ for loading model + playback
'''

parser = argparse.ArgumentParser(description='PLE DQN DDQN MDQN trainer')

parser.add_argument('--hpc', help='Using HPC qsub, how many repated tests', default=0, type=int)
parser.add_argument('--priority', help='Using HPC qsub, priority -1024 to 1024', default=0, type=int)
parser.add_argument('--short', help='Short run, quick test', action='store_true')
parser.add_argument('--play', help='Playback without learning', action='store_true')
parser.add_argument('--comment_name', help='Filename as comment', action='store_true')

parser.add_argument('--load_model', help='Model checkpoint to load', default='last', type=str)
parser.add_argument('--same_rand_seed', help='Same rand_seed as in params', action='store_true')
parser.add_argument('--comment', help='Comment for every run', default='', type=str)
parser.add_argument('--results_db', help='Results file name results_db', default='', type=str)

parser.add_argument('param', help='Path to param CSV or dir of param CSVs', default='', type=str)

args = parser.parse_args()

param_csv_input = ''
if len(args.param) > 1:
    param_csv_input = args.param
    if not param_csv_input.endswith('/') and not param_csv_input.endswith('.csv'):
        param_csv_input += '/'
else:
    raise Exception('Missing param.csv in argument')

len_repeat = 1

param_csv_name = os.path.basename(param_csv_input)
param_csv_name = os.path.splitext(param_csv_name)[0]

is_playback = args.play
is_hpc = args.hpc > 0
is_short_run = args.short
if is_hpc:
    len_repeat = args.hpc

path_base = os.getcwd() + '/'

cur_id = 0
repeat_id = 0
# check against id collision
existing_logs = os.listdir(path_base + 'logs/')
for candidate in existing_logs:
    results = re.search('([0-9]+).log', candidate)
    if results is not None and len(results.groups()):
        id_last = int(results.groups()[0])
        if id_last >= cur_id:
            cur_id = id_last + 1

        with open(path_base + 'logs/' + candidate, 'r') as f:
            first_line = f.readline()
            if first_line.startswith('repeat_id ='):
                repeat_id = max(int(first_line.split('=')[1]), repeat_id)
            else:
                lines = f.readlines()
                for line in lines:
                    match = re.search(r"repeat_id:\ ([0-9]+)", line)
                    if match:
                        repeat_id = max(int(match.group(1)), repeat_id)
if cur_id == 0:
    cur_id = 1

param_csv_arr = []
if param_csv_input.endswith('.csv'):
    param_csv_arr.append(param_csv_input)
else:
    for each in os.listdir(param_csv_input):
        param_csv_arr.append(param_csv_input + each)

params = {}
for param_csv in param_csv_arr:
    with open(param_csv, 'r') as fp:
        for line in fp:
            pair = line.strip().split(';')
            value = str(pair[1].strip())

            if re.match('^[0-9\\-\\+]+$', value):
                value = int(value)
            elif re.match('^[0-9e\\.\\-\\+]+$', value):
                value = float(value)
            elif value.lower() == 'false':
                value = False
            elif value.lower() == 'true':
                value = True

            params[pair[0]] = value
        fp.close()

if len(args.results_db) > 0:
    params['results_db'] = args.results_db

repeat_id += 1

for count_repeat in xrange(len_repeat):
    if is_playback:
        params['is_playback'] = True
        params['is_load_model'] = True
        params['is_save_qmap'] = False
        params['is_verbose'] = True
        params['model_prefix'] = args.load_model
    else:
        params['id'] = cur_id
        params['repeat_id'] = repeat_id

    if is_short_run:
        params['frames_per_epoch'] = 3e1
        #params['is_verbose'] = True
        params['is_save_qmap'] = False
        params['frames_update_target_network'] = 2e1
        params['frames_before_start_training'] = 1e1

    if not args.same_rand_seed:
        params['rand_seed'] = 0

    if len(args.comment) > 0:
        params['comment'] = args.comment
    elif args.comment_name:
        params['comment'] = param_csv_name

    str_params = param_utils.format_params(params)

    if not is_playback:
        with open(path_base + 'logs/' + str(params['id']) + '.log', 'a') as fp:
            fp.write('repeat_id = {}\n'.format(repeat_id))
            fp.close()

    if is_hpc:
        hpc_script = 'hpc_cpu'
        if int(params['gpu']) > 0:
            hpc_script = 'hpc'

        print 'id:', params['id']
        cmd = ["qsub", '-N', params['results_db'], '-p', str(args.priority), '-v str_params={},path_base={}'.format(str_params, path_base), path_base + hpc_script + ".sh"]
        subprocess.call(cmd, shell=False)
    else:
        sys.argv[1] = str_params
        import main


    cur_id += 1