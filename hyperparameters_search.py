from sklearn.model_selection import ParameterGrid
import subprocess
import traceback
import logging
import os
import re
import sys
import time
import param_utils
import copy
import json
import numpy as np

'''
Script for qsub cluster param search
'''

max_gpu_scripts = 400 # useful to improve performance for RNN-ConNets
count_test_repeat = 10 # Algorithm Stability Tests , repeated tests for NON const_rand_seed
debug_is_export_dic_commment = False

limit_count_from = 0
limit_count_to = sys.maxsize #511

if(len(sys.argv) > 1):
    limit_count_from = int(sys.argv[1])
    limit_count_to = int(sys.argv[2])


name = 'maze-comp-mod'
#      '123456789012345'
param_utils.init_log('./logs/{}.log'.format(name))

logging.info('idx from: {} to: {}'.format(limit_count_from, limit_count_to))

param_sequential = [[
        { 'comment': 'dqn', 'utility_function': 'dqn', 'target_network_alpha' : 1.0},
        { 'comment': 'ddqn', 'utility_function': 'ddqn_target', 'target_network_alpha' : 1.0},
        { 'comment': 'mdqn2_1_0', 'utility_function': 'mdqn', 'mdqn_models': 2, 'mdqn_avg': 'average', 'target_network_alpha' : 1.0},
        { 'comment': 'mdqn2_0_0', 'utility_function': 'mdqn', 'mdqn_models': 2, 'mdqn_avg': 'average', 'target_network_alpha' : 0.0},
        { 'comment': 'mdqn3_1_0', 'utility_function': 'mdqn', 'mdqn_models': 3, 'mdqn_avg': 'average', 'target_network_alpha' : 1.0},
        { 'comment': 'mdqn3_0_0', 'utility_function': 'mdqn', 'mdqn_models': 3, 'mdqn_avg': 'average','target_network_alpha': 0.0}
    ]
]

param_temp = [
    [
        {'comment': 'Pixels input: Grayscale', 'is_train_from_pixels': True, 'is_grayscale_pixels': True,
         'is_use_tiny_convnet': False, 'gpu': 1},
        {'comment': 'Pixels input: RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
         'is_use_tiny_convnet': False, 'gpu': 1},
        {'comment': 'Pixels input: Tiny, RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
         'is_use_tiny_convnet': True, 'gpu': 1},
        {'comment': 'Pixels input: Diff., RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
         'is_use_tiny_convnet': False, 'is_state_as_difference': True, 'gpu': 1},
        {'comment': 'Pixels input: Diff., Tiny, RGB', 'is_train_from_pixels': True,
         'is_grayscale_pixels': False, 'is_use_tiny_convnet': True, 'is_state_as_difference': True, 'gpu': 1}
    ],
    [
        { 'comment': 'Default'},

        { 'comment': 'RNN: GRU', 'nn_type': 'gru' },
        { 'comment': 'RNN: LSTM', 'nn_type': 'lstm' },
        { 'comment': 'RNN: ELU', 'nn_type': 'elu' },
        { 'comment': 'RNN: SELU', 'nn_type': 'selu' },
        { 'comment': 'RNN: LeakyReLU', 'nn_type': 'leakyrelu' },
        # RNN: ReLU

        { 'comment': 'MDQN: median', 'mdqn_avg': 'median'},
        { 'comment': 'MDQN: min', 'mdqn_avg': 'min'},
        { 'comment': 'MDQN: min', 'mdqn_avg': 'max'},
        # MDQN: mean

        {'comment': 'Batch norm: True', 'is_batch_norm': True},
        # Batch norm: False

        {'comment': 'Dropout: 0.1', 'dropout': 0.1},
        # Dropout: 0.0

        {'comment': 'Mini-batch: 64', 'mini_batch_size': 64},
        {'comment': 'Mini-batch: 16', 'mini_batch_size': 16},
        {'comment': 'Mini-batch: 8', 'mini_batch_size': 8},
        # Mini-batch: 32

        {'comment': 'Frames back: 10', 'frames_back': 10},
        # Frames back: 5

        {'comment': 'Reg.: L1 0.01', 'reg': 0.01, 'reg_type': 'l1'},
        {'comment': 'Reg.: L2 0.01', 'reg': 0.01, 'reg_type': 'l2'},
        # Reg.: None

        {'comment': 'Optimizer: Adam', 'opt': 'adam'},
        {'comment': 'Optimizer: SGD', 'opt': 'sgd', 'is_cumulative_reward': False},
        # Optimizer: RMSProp

        {'comment': 'Grad clip.: 10.0', 'grad_clip': 10.0},
        {'comment': 'Grad clip.: 1.0', 'grad_clip': 1.0},
        # Grad clip.: 0.0

        {'comment': 'Beta replay buffer: False', 'is_memory_unbiased': False},
        # Beta replay buffer: True

        {'comment': 'Replay buffer: 1e6', 'frames_in_replay_memory': 1e6},
        # Replay buffer: 5e5

        {'comment': 'Frames before: 1e5', 'frames_before_start_training': 1e5},
        # Frames before: 5e4

        {'comment': 'Bellman Gamma: 0.90', 'bellman_gamma': 0.9},
        {'comment': 'Bellman Gamma: 0.80', 'bellman_gamma': 0.8},
        # Bellman Gamma: 0.99

        {'comment': 'Dueling arch.: True', 'dueling_network': True},
        # Dueling arch.: False

        {'comment': 'Cumulative reward: False', 'is_cumulative_reward': False},
        # Cumulative reward: True

        {'comment': 'Model: n states to n act.', 'architecture': 'n_to_n'},
        # Model: 1 states to n actions

# important
        {'comment': 'Target network alpha: 0.0', 'target_network_alpha': 0.0},
        {'comment': 'Target network alpha: 0.5', 'target_network_alpha': 0.5},
        # Target network alpha: 1.0


        # {'comment': 'Pixels input: Grayscale', 'is_train_from_pixels': True, 'is_grayscale_pixels': True,
        #  'is_use_tiny_convnet': False},
        # {'comment': 'Pixels input: RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
        #  'is_use_tiny_convnet': False},
        # {'comment': 'Pixels input: Tiny ConvNet Grayscale', 'is_train_from_pixels': True, 'is_grayscale_pixels': True,
        #  'is_use_tiny_convnet': True},
        # {'comment': 'Pixels input: Tiny ConvNet RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
        #  'is_use_tiny_convnet': True},
        # {'comment': 'Pixels input: Diff. Grayscale', 'is_train_from_pixels': True, 'is_grayscale_pixels': True,
        #  'is_use_tiny_convnet': False, 'is_state_as_difference': True},
        # {'comment': 'Pixels input: Diff. RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
        #  'is_use_tiny_convnet': False, 'is_state_as_difference': True},
        # {'comment': 'Pixels input: Diff. Tiny ConvNet Grayscale', 'is_train_from_pixels': True, 'is_grayscale_pixels': True,
        #  'is_use_tiny_convnet': True, 'is_state_as_difference': True},
        # {'comment': 'Pixels input: Diff. Tiny ConvNet RGB', 'is_train_from_pixels': True, 'is_grayscale_pixels': False,
        #  'is_use_tiny_convnet': True, 'is_state_as_difference': True},
        # Pixels input: None

        {'comment': 'Diff. states: True', 'is_state_as_difference': True},
        # Diff. states: False

        # TODO
#        {'comment': 'Priority replay buffer: Skewed', 'experience_type': 'skewed'},
        {'comment': 'Priority replay buffer: Proportional', 'experience_type': 'proportional'},
#        {'comment': 'Priority replay buffer: Random', 'experience_type': 'random'},
        # Priority replay buffer: Ranked

        {'comment': 'State prev. act., reward: True', 'is_use_previous_action_in_state': True},
        # State prev. act., reward: False

#        {'comment': 'Action nothing: False', 'is_add_noop_action': False},
        # Action nothing: True

        {'comment': 'Extra frame reward: 0.0', 'reward_extra_per_frame': 0.0},
        # Extra frame reward: 1e-5

        {'comment': 'Terminal reward: 0.0', 'reward_terminal_state': 0.0},
        # Terminal reward: -1e3

        {'comment': 'Epsilon start,end: 1e-1, 1e-6', 'epsilon_explore_start': 1e-1},
        {'comment': 'Epsilon start,end: 1e-3, 1e-3', 'epsilon_explore_end': 1e-3},
        {'comment': 'Epsilon start,end: 1e-1, 1e-1', 'epsilon_explore_start': 1e-1, 'epsilon_explore_end': 1e-1},
        # Epsilon start,end: 1e-3, 1e-6

        {'comment': 'Epsilon greedy: False', 'is_e_greedy': False},
        # Epsilon greedy: True

        {'comment': 'Epsilon stuck: True', 'is_e_stuck': True},
        # Epsilon stuck: False

        {'comment': 'SARSA: True', 'is_sarsa': True},
        # SARSA: False

        {'comment': 'Online: True', 'is_online': True, 'is_cumulative_reward': False},
        # Online: False

        {'comment': 'Offline prebatch: True', 'is_offline_pre_batch': True},
        # Offline prebatch: False

# TODO
        #{'comment': 'Curriculum: True', 'is_curriculum_learning': True},
        # Curriculum: False
    ],
];

param_grid = {

    'lr': [1e-4, 1e-5, 1e-6],
}

param_const = {
    'results_db': name,

    'game': 'raycastmaze', # raycastmaze pong flappybird

    'is_train_from_pixels': True,
    'is_grayscale_pixels': True,
    'is_rg_pixels': True,
    'is_state_as_difference': True,
    'is_use_tiny_convnet': False,
    'is_curriculum_learning': True,
    'reward_extra_per_frame': 0.0,
    'is_add_noop_action': False,
    'actions_to_ignore': 'backward',

    'is_save_qmap': True,
    'qmap_epoch_interval': 10,
    'qmap_min_score': 0,
    'rand_seed': 0,

    'evaluation_stride': 20,
    'max_nb_epochs': 100,
    'nb_epochs_end_of_learning': 100,

    'gpu': 1,
    'cpu': 8,
    'cpu_threads': 16,
    'is_playback': False,
    'is_load_model': False,
    'is_verbose': False
}


cur_id = 1 # start id for tests, if 0 then automatic (dangerous when multiple grid searches in parallel)

complete = []

grid = []
if len(param_grid.items()) > 0:
    grid = ParameterGrid(param_grid)

sequences = []
tmp_sequence = []
for each_seq in param_sequential:
    tmp_sequence = copy.deepcopy(sequences)
    sequences = []
    for each in each_seq:
        if len(tmp_sequence) > 0:
            for each_inner in tmp_sequence:
                sequences.append(dict(each_inner.items() + each.items()))
        else:
            sequences.append(each)


if len(sequences) > 0:
    for each in sequences:
        for params_comb in grid:
            complete.append(dict(each.items() + params_comb.items()))
else:
    complete = grid

path_base = os.getcwd() + '/'

if debug_is_export_dic_commment:
    with open(path_base + '/results/complete.json', 'w') as outfile:
        json.dump(complete, outfile)
    exit(0)

repeat_id = 0
counter = 0

if cur_id == 0:
    cur_id = 1

# check against id collision & repeat_id collision
existing_logs = os.listdir(path_base + 'logs/')
for candidate in existing_logs:
    results = re.search('([0-9]+).log', candidate)
    if results is not None and len(results.groups()):
        id_last = int(results.groups()[0])
        if id_last >= cur_id:
            cur_id = id_last + 1

        with open(path_base + 'logs/' + candidate, 'r') as f:
            first_line = f.readline()
            if first_line.startswith('repeat_id ='):
                repeat_id = max(int(first_line.split('=')[1]), repeat_id)
            else:
                lines = f.readlines()
                for line in lines:
                    match = re.search(r"repeat_id:\ ([0-9]+)", line)
                    if match:
                        repeat_id = max(int(match.group(1)), repeat_id)

count_gpu_scripts = 0
for params_comb in complete:

    repeat_id += 1

    for _ in range(count_test_repeat):
        counter += 1

        if counter < limit_count_from or counter > limit_count_to:
            continue

        param_const['id'] = cur_id
        params = dict(params_comb.items() + param_const.items())

        params['repeat_id'] = repeat_id # count_test_repeat ID when testing random

        # reserve id and repeat_id
        with open(path_base + 'logs/' + str(params['id']) + '.log', 'a') as fp:
            fp.write('repeat_id = {}\n'.format(repeat_id))
            fp.close()

        try:
            str_params = param_utils.format_params(params)

            logging.info('id:{} r:{} {}/{}'.format(cur_id, repeat_id, counter, len(complete) * count_test_repeat))
            logging.info(params)

            if int(params['gpu']) == 0:
                if count_gpu_scripts < max_gpu_scripts:
                    params['gpu'] = 1

            hpc_script = 'hpc_cpu'
            if int(params['gpu']) > 0:
                hpc_script = 'hpc'
                count_gpu_scripts += 1

            cmd = ["qsub", '-N', name, '-v str_params={},path_base={}'.format(str_params, path_base), path_base + hpc_script + ".sh"]
            logging.info(' '.join(cmd))
            stdout = subprocess.check_output(cmd, shell=False)
            logging.info(stdout)
        except:
            traceback.print_exc()

        time.sleep(1)
        cur_id += 1
