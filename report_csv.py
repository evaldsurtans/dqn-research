import numpy as np
import re
import sys
import copy
import argparse

parser = argparse.ArgumentParser(description='CSV aggregate to combined results')

# as an list input
parser.add_argument('--group', default='comment', help='comment repeat_id etc.', nargs='+')
parser.add_argument('--select', help='extra selection arguments', nargs='*')

parser.add_argument('--fix_comments', action='store_true')
parser.add_argument('--fix_repeat_id', help='overwrite repeat_id for every group', action='store_true')

parser.add_argument('--output', help='Output CSV', default='', type=str)
parser.add_argument('--suffix', help='Output CSV Suffix', default='combined', type=str)

parser.add_argument('input', help='Path to results CSV', default='', type=str)

args = parser.parse_args()

path_input = args.input
path_output = args.output
if path_output == '':
    path_output = path_input[:-4] + '_' + args.suffix + path_input[-4:]

path_output_repeat_id = path_input[:-4] + '_repeat_id_fixed'+ path_input[-4:]

group_by = args.group
if not isinstance(group_by, list):
    group_by = [group_by]

group_func =  ['max',       'avg',      'med',       'var',       'med',          'avg',      'var',      'avg',          'avg']
group_param = ['eval_max',  'eval_max', 'eval_max',  'eval_max',  'eval_avg',     'eval_avg', 'eval_avg', 'time_total',   'time_avg']

order_by = 'med(eval_avg)'
order_dir = 'desc'

select = ['id', 'count', 'comment', 'repeat_id', 'lr']
extra_select = args.select
if extra_select:
    select += extra_select

# add missing select items from group_by
for key in group_by:
    if not key in select:
        select.append(key)

# comment formating options
comments_default = [
    'RNN: ReLU',
    'Batch norm: False',
    'Dropout: 0.0',
    'Mini-batch: 32',
    'Frames back: 5',
    'Reg.: None',
    'Optimizer: RMSProp',
    'Grad clip.: 0.0',
    'Beta replay buffer: True',
    'Replay buffer: 5e5',
    'Frames before: 5e4',
    'Bellman Gamma: 0.99',
    'Dueling arch.: False',
    'Cumulative reward: True',
    'Model: 1 states to n actions',
    'Target network alpha: 1.0',
    'Pixels input: None',
    'Diff. states: False',
    'State prev. act., reward: False',
    'Extra frame reward: 1e-5',
    'Terminal reward: -1e3',
    'Epsilon start,end: 1e-3, 1e-6',
    'Epsilon greedy: True',
    'Epsilon stuck: False',
    'SARSA: False',
    'Online: False',
    'Offline prebatch: False',
    'priority replay buffer: ranked'
]
comments_replace = [
    ['Adam', 'Optimizer: Adam'],
    ['SGD', 'Optimizer: SGD'],
    ['n states to n actions', 'Model: n states to n act.'],
    ['Epsilon start: 1e-1', 'Epsilon start,end: 1e-1, 1e-6'],
    ['Epsilon start,end: 1e-3', 'Epsilon start,end: 1e-3, 1e-3'],
    ['Epsilon start,end: 1e-1', 'Epsilon start,end: 1e-1, 1e-1'],
    ['batch norm.', 'batch norm: true'],
    ['Reg. L1: 0.01', 'Reg.: L1 0.01'],
    ['Reg. L2: 0.01', 'Reg.: L2 0.01'],
    ['RNN GRU', 'RNN: GRU'],
    ['RNN LSTM', 'RNN: LSTM'],
    ['RNN SELU', 'RNN: SELU'],
    ['RNN ELU', 'RNN: ELU'],
    ['RNN LeakyReLU', 'RNN: LeakyReLU']
]

def func_compile(data, func):
    result = 0
    if func == 'avg':
        result = np.average(data)
    elif func == 'var':
        result = np.var(data)
    elif func == 'max':
        result = np.max(data)
    elif func == 'min':
        result = np.min(data)
    elif func == 'med':
        result = np.median(data)
    return result

lines = open(path_input).readlines()
rows = []
rows_numeric = []
header = None
seperator = ';'

repeat_id_max = 0

if lines[0].index(seperator) < 0:
    seperator = ','

for line in lines:
    row = line.strip().split(seperator)

    # last empty element
    if len(row[-1].strip()) == 0:
        row = row[:-1]

    if header is None:
        header = row
    else:
        if len(row) != len(header):
            continue
        formated = []
        formated_numeric = []
        for each in row:
            if re.match('^[0-9E\+\-\.]+$', each):
                formated.append(float(each))
                formated_numeric.append(float(each))
            else:
                formated.append(each)
                formated_numeric.append(0)
        rows.append(formated)
        rows_numeric.append(formated_numeric)


def get_group_by_key(each):
    result = []
    for key in group_by:
        result.append(str(each[header.index(key)]))
    return '_'.join(result)

def get_group_param_name(idx_group):
    return group_func[idx_group] + '(' + group_param[idx_group] + ')'

rows = sorted(rows, key=lambda each: get_group_by_key(each))

# same order of grouped results for numeric rows
temp = []
for row in rows:
    idx_id = header.index('id')
    id = float(row[idx_id])
    row_numeric = [it for it in rows_numeric if it[idx_id] == id][0]
    temp.append(row_numeric)
rows_numeric = temp

# add values to arrays
grouped_results = []
last_group_by_value = ''
for idx_row, row in enumerate(rows):

    row_numeric = rows_numeric[idx_row]
    group_by_value = get_group_by_key(row)

    repeat_id_max = max(repeat_id_max, row_numeric[header.index('repeat_id')])

    if last_group_by_value != group_by_value:
        last_group_by_value = group_by_value

        grouped_results.append({
            'group_by_value': group_by_value, # group by name synthetic all group parts combined
            'grouped_values': {}, # all values before sum/avg
            'selection': {}, # static selected values + baked values
            'ids': [], # ids included
        })
        grouped_results_last = grouped_results[-1]

        for idx_group in xrange(len(group_param)):
            grouped_results_last['grouped_values'][get_group_param_name(idx_group)] = []

        # selection contains first record of group
        for each in select:
            # if statement
            if each in header: #skip non-existing keys like "count"
                value = row[header.index(each)]

                # fix comments
                if args.fix_comments and each == 'comment':
                    replace_select = [it for it in comments_replace if it[0].lower().strip() == value.lower().strip()]
                    if len(replace_select):
                        replace_select = replace_select[0]
                        value = value.lower().replace(replace_select[0].lower(), replace_select[1])

                grouped_results_last['selection'][each] = value

    grouped_results_last = grouped_results[-1]
    grouped_results_last['ids'].append(row_numeric[header.index('id')])

    for idx_group in xrange(len(group_param)):
        key = get_group_param_name(idx_group)
        key_header = group_param[idx_group]
        grouped_results_last['grouped_values'][key].append(row_numeric[header.index(key_header)])

# calculate averages
for grouped_result in grouped_results:
    for idx_group in xrange(len(group_param)):
        key = get_group_param_name(idx_group)
        value = grouped_result['grouped_values'][key]
        func = group_func[idx_group]
        res = func_compile(value, func)
        grouped_result['selection'][key] = res
    # count runs
    grouped_result['selection']['count'] = len(grouped_result['grouped_values'][get_group_param_name(0)])

# add default comment duplicates
idx = 0
while idx < len(grouped_results):
    grouped_result = grouped_results[idx]
    if grouped_result['selection']['comment'] == 'default':
        for each in comments_default:
            copied = copy.deepcopy(grouped_result)
            copied['selection']['comment'] = each
            grouped_results.append(copied)
        grouped_results.remove(grouped_result)
    else:
        idx += 1


# sort
grouped_results = sorted(grouped_results, key=lambda each: each['selection'][order_by], reverse=(order_dir == 'desc'))

if args.fix_repeat_id:
    for grouped_result in grouped_results:
        repeat_id_max += 1
        grouped_result['repeat_id'] = repeat_id_max

    with open(path_output_repeat_id, 'w') as f:
        f.write(';'.join(map(str, header)) + '\n')
        for row in rows:
            id = int(row[header.index('id')])
            grouped_result = [it for it in grouped_results if id in it['ids']]
            if len(grouped_result) != 1:
                print 'Error duplicate id: {}'.format(id)
            row[header.index('repeat_id')] = grouped_result[0]['repeat_id']
            f.write(';'.join(map(str, row)) + '\n')

results = []
# add to CSV results
for grouped_result in grouped_results:

    # add header row
    if len(results) == 0:
        results.append([])
        result_row = results[-1]
        for key in select:
            result_row.append(key)
        for idx_group in xrange(len(group_param)):
            result_row.append(get_group_param_name(idx_group))

    results.append([])
    result_row = results[-1]

    for key in select:
        value = grouped_result['selection'][key]
        if args.fix_repeat_id:
            if key == 'repeat_id':
                value = grouped_result['repeat_id']
        result_row.append(value)

    for idx_group in xrange(len(group_param)):
        key = get_group_param_name(idx_group)
        value = grouped_result['selection'][key]
        result_row.append(value)

with open(path_output, 'w') as f:
    for row in results:
        print '\t\t'.join(map(str, row))
        f.write(';'.join(map(str, row)) + '\n')




