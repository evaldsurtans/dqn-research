import numpy as np
import pickle
import logging
import json
import sys
import math
import fcntl
import os
from copy import copy, deepcopy

from keras.layers import Dense, Activation, SimpleRNN, LSTM, GRU, Flatten, BatchNormalization
from keras.regularizers import l1, l2, l1_l2
from keras.activations import relu, selu, elu
from keras.layers.advanced_activations import LeakyReLU, PReLU
from sklearn import preprocessing

import random
import time
from PIL import Image, ImageDraw #http://effbot.org/imagingbook/image.htm

image_finish = Image.open('./assets/finish.png', 'r')

sys.setrecursionlimit(40000)

def float_to_csv_str(number, digits = 23):
    try:
        if isinstance(number, basestring):
            return number
        try:
            number = float(number)
            return ('%.{}f'.format(digits) % number).rstrip('0').rstrip('.')
        except:
            print 'value = {}'.format(number)
        return number
    except:
        pass
    return 'nan'

def copy_x(x):
    # Fast version of deepcopy
    cx = []
    for i in range(len(x)):
        cx.append(x[i][:])
    return cx

def normalize(v):
    norm=np.linalg.norm(v, ord=1)
    if norm==0:
        norm=np.finfo(v.dtype).eps
    return v/norm

def generate_grad_cam_map(env, model_target, x_input, raw_q_values, params, variables, layer_idx):
    try:
        # sudo pip install keras-vis
        from vis.visualization import visualize_cam

        seed_img = Image.fromarray(env.getScreenRGB())
        seed_img = seed_img.convert('L').convert('RGB')
        seed_img_arr = np.asarray(seed_img).astype('uint8')

        action_idx = np.argmax(raw_q_values)

        # 2 lowest layer after input
        heatmap = visualize_cam(model_target, layer_idx, [action_idx], seed_img_arr, alpha=0.3, input_data_rnn=x_input)
        heatmap_img = Image.fromarray(np.transpose(np.array(heatmap), axes=[1, 0, 2]))
        timestamp = time.time()
        seed_img = Image.fromarray(np.transpose(np.array(env.getScreenRGB()), axes=[1, 0, 2]))

        composite_img = Image.new("RGB", (seed_img.size[0] * 2, seed_img.size[1]))
        composite_img.paste(heatmap_img, (0, 0))
        composite_img.paste(seed_img, (seed_img.size[0], 0))

        action_name = 'none'
        if action_idx == 0:
            action_name = 'up'
        elif action_idx == 3:
            action_name = 'right'
        elif action_idx == 2:
            action_name = 'down'
        elif action_idx == 1:
            action_name = 'left'

        filename = './qmaps/cam-{}-{}-{}-{}-{}-{}.png'.format(params['game'], action_name, params['id'], variables['epochs'], layer_idx, timestamp)

        composite_img.save(filename, 'PNG')
    except:
        pass

def generate_qmap(get_qvalues, params, variables, env, score_in_episode, state, reshape_to_single_timestep_state, x, dimensions_actions):

    const_is_display_positions = False
    temp_arr = None
    q_map = None
    image_background = None
    image_overlay = None
    image_overlay_annotations = None
    max_q = float('-inf')
    min_q = float('inf')
    all_q = []
    bounds = None
    skip_positions = []
    scale_factor = 1

    is_rotate_needed = False
    is_flip_needed = False
    is_transpose_needed = False

    if params['game'] == 'raycastmaze':
        scale_factor = 20
        game_map = np.array(env.game.map_).astype(np.byte)
        temp_arr = np.zeros((game_map.shape[0], game_map.shape[1], 3))
        q_map = np.zeros((temp_arr.shape[0], temp_arr.shape[1], 1))
        pos_finish = (-100,-100)

        for img_x in range(game_map.shape[0]):
            for img_y in range(game_map.shape[1]):

                if game_map[img_x, img_y] != 0:
                    skip_positions.append([img_x, img_y])

                if game_map[img_x, img_y] == 0 or game_map[img_x, img_y] == 2:
                    temp_arr[img_x, img_y] = (255, 255, 255)

                    if game_map[img_x, img_y] == 0:
                        pos_current = np.array([img_x, img_y])
                        env.game.pos = pos_current

                        q_tests = []
                        state_x = []

                        for deg in range(360):
                            X_TURN = np.cos(math.pi / 180.0)
                            Y_TURN = np.sin(math.pi / 180.0)

                            dirX = env.game.dir[0, 0] * X_TURN - env.game.dir[0, 1] * Y_TURN
                            dirY = env.game.dir[0, 0] * Y_TURN + env.game.dir[0, 1] * X_TURN

                            planeX = env.game.plane[0, 0] * \
                                      X_TURN - env.game.plane[0, 1] * Y_TURN
                            planeY = env.game.plane[0, 0] * \
                                      Y_TURN + env.game.plane[0, 1] * X_TURN

                            env.game.dir[0, 0] = dirX
                            env.game.dir[0, 1] = dirY
                            env.game.plane[0, 0] = planeX
                            env.game.plane[0, 1] = planeY

                            # simulate rotation around the centre of pos
                            env.game.step(0)
                            env._draw_frame()

                            state = None
                            # high dimension representation of game (from visible pixels)
                            if params['is_grayscale_pixels']:
                                # numpy array with the shape (width, height)
                                grayscale_state = np.array(env.getScreenGrayscale())
                                state = np.reshape(grayscale_state,
                                                   (grayscale_state.shape[0], grayscale_state.shape[1], 1))
                            elif params['is_rg_pixels']:
                                # numpy array with the shape (width, height, 2)
                                state = convertScreenRG(env.getScreenRGB())
                            else:
                                # numpy array with the shape (width, height, 3)
                                state = np.array(env.getScreenRGB())

                            state_x.append(state)
                            if len(state_x) >= params['frames_back']:
                                state_x = state_x[-params['frames_back']:]
                                x_input = reshape_to_single_timestep_state(state_x)
                                q_values = get_qvalues(x_input)
                                q_test = np.max(q_values)
                                q_tests.append(q_test)

                        q_test = np.max(np.array(q_tests))
                        max_q = max(max_q, q_test)
                        min_q = min(min_q, q_test)
                        all_q.append(q_test)
                        q_map[img_x, img_y] = q_test



                if game_map[img_x, img_y] == 2:
                    pos_finish = (int(img_y * scale_factor + scale_factor * 0.2), int(img_x * scale_factor + scale_factor * 0.2))

                if const_is_display_positions:
                    if game_map[img_x, img_y] ==  2:
                        temp_arr[img_x, img_y] = (255, 0, 0)
                    elif math.floor(env.game.pos[0][1]) == img_x and math.floor(env.game.pos[0][1]) == img_y:
                        temp_arr[img_x, img_y] = (0, 255, 0)

        image_overlay_annotations = Image.new('RGBA',
                                              (temp_arr.shape[0] * scale_factor, temp_arr.shape[1] * scale_factor),
                                              (255, 255, 255, 0))

        image_overlay_annotations.paste(image_finish, pos_finish)

        temp_arr = temp_arr.astype(np.uint8)
        image_background = Image.fromarray(temp_arr)

    elif params['game'] == 'pong':
        scale_factor = 10
        is_transpose_needed = True

        # 57 is opponent paddle
        # avoid bounce info inside state vector
        if x[-1][0] >= 45 and x[-1][3] < 0:
            # ball on opponent's pad and comming at our direction

            temp_arr = np.array(env.getScreenRGB())
            image_background = Image.fromarray(np.transpose(env.getScreenRGB(), axes=[1,0,2]))
            q_map = np.zeros((temp_arr.shape[0], temp_arr.shape[1], 1))

            # draw dir vector
            image_overlay_annotations = Image.new('RGBA', (temp_arr.shape[0] * scale_factor, temp_arr.shape[1] * scale_factor), (255, 255, 255, 0))
            draw = ImageDraw.Draw(image_overlay_annotations)
            line_start = np.array([x[-1][0], x[-1][1]])
            velocity = preprocessing.normalize(np.array([x[-1][3], x[-1][2]]), norm='l2')
            line_end = line_start + velocity * 4
            line_start *= scale_factor
            line_end *= scale_factor
            radius = 3
            delta_x_pad = 10
            line_start[0] -= delta_x_pad
            line_end[0] -= delta_x_pad
            draw.ellipse([(line_start[0] - radius, line_start[1] - radius), (line_start[0] + radius, line_start[1] + radius)], fill=(0, 0, 0, 255))
            draw.line([(line_start[0], line_start[1]), (line_end[0][0], line_end[0][1])], fill=(0, 0, 0, 255), width=3)

            # 64x48
            # 0 ball x position.
            # 1 ball y position.
            # 2 ball y velocity.
            # 3 ball x velocity.
            # 4 players velocity.
            # 5 cpu y position.
            # 6 player y position.

            bounds = {
                'top': 0,
                'left': 0,
                'right': int(math.floor(x[-1][0])),
                'bottom': 48
            }

            state = env.getGameState().tolist()
            x_pos_last = None
            x_pos = int(round(state[0]))
            x_pos_till_act = x_pos
            clone_x = copy_x(x)

            diff_len = 0
            diff_idx = 0
            diff_clone_x = None
            last_clone_x = None

            min_x_pos = 10
            max_x_pos = int(clone_x[-1][0])

            for img_x in range(min_x_pos, max_x_pos + 1):
                for img_y in range(0, bounds['bottom']):

                    clone_delta_x = copy_x(clone_x)

                    # timesteps before
                    for idx, step_x in enumerate(clone_delta_x):
                        step_x[6] = img_y - 3 # paddle offset
                        step_x[4] = 0
                        #step_x[2] = 0
                        #step_x[3] = 0
                        step_x[0] -= (img_x - min_x_pos)  #simulate ball proximity from far to close

                    x_input = reshape_to_single_timestep_state(clone_delta_x)
                    q_values = get_qvalues(x_input)
                    q_test = np.max(q_values)

                    max_q = max(max_q, q_test)
                    min_q = min(min_q, q_test)
                    all_q.append(q_test)
                    q_map[img_x, img_y] = q_test
            pass


    elif params['game'] == 'catcher':
        scale_factor = 5
        is_rotate_needed = True
        is_flip_needed = True

        if x[-1][3] >= 0 and x[-1][3] <= 10 and abs(x[-1][0] - x[-1][2]) > 16:
            # fruit at top of the screen and enough distance between fruit and catcher to evaluate

            temp_arr = env.getScreenRGB()
            image_background = Image.fromarray(env.getScreenRGB())

            q_map = np.zeros((temp_arr.shape[0], temp_arr.shape[1], 1))
            # 0 fruits x position
            # 1 players velocity
            # 2 player x position
            # 3 fruits y position

            for img_x in range(0, temp_arr.shape[0]):
                for img_y in range(0, temp_arr.shape[1]):
                    clone_x = copy_x(x)

                    # timesteps before
                    for idx, step_x in enumerate(clone_x):
                        step_x[2] = img_x
                        step_x[1] = 0
                        step_x[0] = clone_x[-1][0]
                        step_x[3] = img_y - (len(clone_x) - 1 - idx)

                    x_input = reshape_to_single_timestep_state(clone_x)
                    q_values = get_qvalues(x_input)
                    q_test = np.max(q_values)

                    max_q = max(max_q, q_test)
                    min_q = min(min_q, q_test)
                    all_q.append(q_test)
                    q_map[img_x, img_y] = q_test




    elif params['game'] == 'flappybird':
        const_bird_x_pos = 70
        const_bird_y_pos = 10
        const_bird_x_delta_pipes = 40
        const_pipe_distance = 144
        temp_arr = env.getScreenRGB()
        is_rotate_needed = True
        is_flip_needed = True

        if (params['qmap_min_score'] > 0 and score_in_episode >= params['qmap_min_score']) or \
                (params['qmap_min_score'] == 0 and state[2] <= temp_arr.shape[0] - const_bird_x_pos - 100):
            # update state
            state = env.getGameState().tolist()
            image_background = Image.fromarray(env.getScreenRGB())
            q_map = np.zeros((temp_arr.shape[0], temp_arr.shape[1], 1))
            """
            dict
                0* player y position.
                1* players velocity.
                2* next pipe distance to player
                3* next pipe top y position
                4* next pipe bottom y position
                5* next next pipe distance to player
                6* next next pipe top y position
                7* next next pipe bottom y position
            """

            for img_x in range(0, temp_arr.shape[0]):
                for img_y in range(0, temp_arr.shape[1]):

                    if const_is_display_positions:
                        if (state[0] + const_bird_y_pos == img_y):
                            temp_arr[img_x, img_y] = (255, 0, 0)
                        if (const_bird_x_pos == img_x):
                            temp_arr[img_x, img_y] = (255, 0, 0)

                        if (state[2] + const_bird_x_pos - const_bird_x_delta_pipes == img_x):
                            temp_arr[img_x, img_y] = (255, 255, 0)
                        if (state[3] == img_y):
                            temp_arr[img_x, img_y] = (255, 255, 0)
                        if (state[4] == img_y):
                            temp_arr[img_x, img_y] = (255, 255, 0)

                        if (state[5] + const_bird_x_pos - const_bird_x_delta_pipes == img_x):
                            temp_arr[img_x, img_y] = (0, 0, 255)
                        if (state[6] == img_y):
                            temp_arr[img_x, img_y] = (0, 0, 255)
                        if (state[7] == img_y):
                            temp_arr[img_x, img_y] = (0, 0, 255)
                    else:

                        clone_x = copy_x(x)
                        for step_x in clone_x:
                            step_x[0] = img_y # 0* player y position.

                            next_pipe_x =  step_x[2] - (img_x - const_bird_x_pos)
                            next_pipe_y1 = step_x[3]
                            next_pipe_y2 = step_x[4]

                            following_pipe_x = step_x[5] - (img_x - const_bird_x_pos)
                            following_pipe_y1 = step_x[6]
                            following_pipe_y2 = step_x[7]

                            if(next_pipe_x <= -10 and score_in_episode >= 1):
                                next_pipe_x = following_pipe_x
                                next_pipe_y1 = following_pipe_y1
                                next_pipe_y2 = following_pipe_y2

                                following_pipe_x += const_pipe_distance


                            step_x[2] = next_pipe_x
                            step_x[3] = next_pipe_y1
                            step_x[4] = next_pipe_y2
                            step_x[5] = following_pipe_x
                            step_x[6] = following_pipe_y1
                            step_x[7] = following_pipe_y2

                        x_input = reshape_to_single_timestep_state(clone_x)
                        q_values = get_qvalues(x_input)
                        q_test = np.max(q_values)

                        max_q = max(max_q, q_test)
                        min_q = min(min_q, q_test)
                        all_q.append(q_test)
                        q_map[img_x, img_y] = q_test

    if not image_background is None:

        delta_q = max_q - min_q

        median = np.median(np.array(all_q)) - min_q
        mid_point = median / delta_q

        print 'median mid: {}'.format(mid_point)

        delta_q_mid = delta_q * mid_point

        # color_red = np.array([255.0, 0.0, 0.0])
        # color_yellow = np.array([255.0, 255.0, 0.0])
        # color_green = np.array([0.0, 255.0, 0.0])

        if bounds is None:
            bounds = {
                'top': 0,
                'left': 0,
                'right': temp_arr.shape[0],
                'bottom': temp_arr.shape[1]
            }

        for img_x in range(bounds['left'], bounds['right']):
            for img_y in range(bounds['top'], bounds['bottom']):
                if not [img_x, img_y] in skip_positions:
                    q_test = q_map[img_x, img_y]

                    q_test = max(min_q, q_test[0]) - min_q

                    val_r = max(0.0, min(1.0, 1.0 - ((q_test - delta_q_mid) / delta_q_mid)))
                    val_g = max(0.0, min(1.0, (q_test / delta_q_mid)))
                    color = (
                        255.0 * val_r,
                        255.0 * val_g,
                        0.0
                    )

                    temp_arr[img_x, img_y] = color

        print("max_q: {} min_q: {}".format(max_q, min_q))


        if is_transpose_needed:
            temp_arr = np.transpose(temp_arr, axes=[1,0,2])

        image_overlay = Image.fromarray(temp_arr, mode='RGB')

        if is_transpose_needed:
            # transpose back for scaling dimensions
            temp_arr = np.transpose(temp_arr, axes=[1,0,2])

        if not scale_factor == 1:
            image_overlay = image_overlay.resize((temp_arr.shape[0] * scale_factor, temp_arr.shape[1] * scale_factor))
            image_background = image_background.resize((temp_arr.shape[0] * scale_factor, temp_arr.shape[1] * scale_factor))

        image = Image.blend(image_background, image_overlay, 0.8) # percent of color overlay

        if is_flip_needed:
            image = image.transpose(Image.FLIP_LEFT_RIGHT)
        if is_rotate_needed:
            image = image.transpose(Image.ROTATE_90)

        if not image_overlay_annotations is None:
            image = image.convert('RGBA')
            image.paste(image_overlay_annotations, (0, 0), image_overlay_annotations)

        filename = './qmaps/{}-{}-{}.png'.format(params['game'], params['id'], variables['epochs'])
        image.save(filename, 'PNG')

        variables['epoch_is_qmap_generated'] = True
        if params['qmap_epoch_interval'] == 0 or params['is_playback']:
            exit()

        return True
    return False


def process_game_low_dimensions_state(state):
    values = []
    for key in state.keys():
        values.append(state[key])
    return np.array(values).astype(np.float32)

def process_waterworld_low_dimensions_state(state):
    state_list = list((state['player_x'], state['player_y'], state['player_velocity_x'], state['player_velocity_y']))
    max_count_elements = 5

    creep_good = []
    creep_bad = []

    creep_pos_good_dist = []
    creep_pos_good_x = []
    creep_pos_good_y = []
    creep_pos_bad_dist = []
    creep_pos_bad_x = []
    creep_pos_bad_y = []

    for str_type, collection in [('GOOD', creep_good), ('BAD', creep_bad)]:
        for i in range(len(state['creep_dist'][str_type])):
            collection.append([ state['creep_dist'][str_type][i], state['creep_pos'][str_type][i][0], state['creep_pos'][str_type][i][1] ])

        #sort by closest
        sorted(collection, key=lambda each: each[0])

    for i in range(max_count_elements):
        if i < len(creep_good):
            creep_pos_good_dist.append(creep_good[i][0])
            creep_pos_good_x.append(creep_good[i][1])
            creep_pos_good_y.append(creep_good[i][2])
        else:
            creep_pos_good_dist.append(-1)
            creep_pos_good_x.append(-1)
            creep_pos_good_y.append(-1)
        if i < len(creep_bad):
            creep_pos_bad_dist.append(creep_bad[i][0])
            creep_pos_bad_x.append(creep_bad[i][1])
            creep_pos_bad_y.append(creep_bad[i][2])
        else:
            creep_pos_bad_dist.append(-1)
            creep_pos_bad_x.append(-1)
            creep_pos_bad_y.append(-1)

    state_list += creep_pos_good_dist
    state_list += creep_pos_good_x
    state_list += creep_pos_good_y

    state_list += creep_pos_bad_dist
    state_list += creep_pos_bad_x
    state_list += creep_pos_bad_y

    return np.array(state_list).astype(np.float32)


def process_snake_low_dimensions_state(state):
    state_list = list((state['snake_head_x'], state['snake_head_y'], state['food_x'], state['food_y']))
    max_count_elements = 20 #if snake longer learning will fail
    snake_elements_x = []
    snake_elements_y = []

    # skip head, because it is already included
    snake_body_pos = state['snake_body_pos'][1:]
    count_elements = len(snake_body_pos)
    for i in range(max_count_elements):
        if i < count_elements:
            snake_elements_x.append(snake_body_pos[i][0])
            snake_elements_y.append(snake_body_pos[i][1])
        else:
            snake_elements_x.append(-1)
            snake_elements_y.append(-1)

    state_list += snake_elements_x
    state_list += snake_elements_y

    return np.array(state_list).astype(np.float32)


def process_flappy_bird_low_dimensions_state(state):
    """
            dict
                0 * player y position.
                1 * players velocity.
                2 * next pipe distance to player
                3 * next pipe top y position
                4 * next pipe bottom y position
                5 * next next pipe distance to player
                6 * next next pipe top y position
                7 * next next pipe bottom y position
            """
    # pprint.pprint(state)
    return np.array( [state['player_y'],
                           state['player_vel'],
                           state['next_pipe_dist_to_player'],
                           state['next_pipe_top_y'],
                           state['next_pipe_bottom_y'],
                           state['next_next_pipe_dist_to_player'],
                           state['next_next_pipe_top_y'],
                           state['next_next_pipe_bottom_y']] ).astype(np.float32)

def leakyrelu(x):
    return  relu(x, alpha=0.3)

def process_layer(layer, params, is_load_activation = False):
    #WeightRegularizer() aims to regularize the weight of the layer
    #ActivityRegularizer() aims to regularize the output of the layer

    if params['reg'] > 0:
        regularizers = []

        for _ in range(3):
            if params['reg_type'] == 'l1':
                regularizers.append(l1(params['reg']))
            elif params['reg_type'] == 'l2':
                regularizers.append(l2(params['reg']))
            elif params['reg_type'] == 'l1l2':
                regularizers.append(l1_l2(params['reg']))

        if len(regularizers) > 0:
            if isinstance(layer, Dense):
                #layer = Dense()
                layer.kernel_regularizer = regularizers[0]
            elif isinstance(layer, LSTM):
                #layer = LSTM()
                layer.kernel_regularizer = regularizers[0]
                layer.recurrent_regularizer = regularizers[1]
            elif isinstance(layer, GRU):
                #layer = GRU()
                layer.kernel_regularizer = regularizers[0]
                layer.recurrent_regularizer = regularizers[1]

    if is_load_activation:
        if isinstance(layer, Dense):
            if params['nn_type'] == 'relu':
                layer.activation = relu
            elif params['nn_type'] == 'selu':
                layer.activation = selu
            elif params['nn_type'] == 'elu':
                layer.activation = elu
            elif params['nn_type'] == 'leakyrelu':
                layer.activation = leakyrelu

    return layer


def save_function(f, func):
    pickle.dump(func, open(f, "wb"))

def load_function(f):
    func = pickle.load(open(f, "rb"))
    return func

def save_model(f, model):
    ps = {}
    for p in model.params:
        ps[p.name] = p.get_value()
        pickle.dump(ps, open(f, "wb"))

def load_model(f, model):
    if sys.version_info[0] < 3:
        ps = pickle.load(open(f, "rb"))
    else:
        u = pickle._Unpickler(open(f, "rb"))
        u.encoding = 'latin1'
        ps = u.load()
    for p in model.params:
        p.set_value(ps[p.name])
    return model

def lock_file(f):
    while True:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            break
        except IOError as e:
            logging.info('results file locked')
            time.sleep(0.1)

def unlock_file(f):
    fcntl.flock(f, fcntl.LOCK_UN)

def convertScreenRG(frame):
    frame = [ frame[:, :, 0], frame[:, :, 1] ]
    frame = np.transpose(frame, (2,1,0))
    frame = np.round(frame).astype(np.uint8)
    return frame