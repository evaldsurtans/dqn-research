#!/bin/sh
#PBS -o /mnt/home/evaldsu/log-info
#PBS -e /mnt/home/evaldsu/log-error
#PBS -q rudens
#PBS -l nodes=1:ppn=8:gpus=1,mem=60g,walltime=128:00:00

#module load cuda/cuda-7.5
module load cuda/cuda-8.0
export TMPDIR=$HOME/tmp
export PYTHONPATH=$PYTHONPATH:$HOME/Documents/prioritized-experience-replay:$HOME/Documents/PyGame-Learning-Environment
source $HOME/machinelearn/bin/activate
cd "${path_base}"
python main.py ${str_params}