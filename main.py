import os
import sys
import json
import re
import logging
import time
import numpy as np
import math
from copy import deepcopy
import fcntl
import random

os.environ['KERAS_BACKEND'] = 'tensorflow'

import param_utils


# Load parameters from 2nd argument in command line
# example format: ^nn_type^:^relu^*^frames_back^:5*^lr^:0.001*^reg^:0.01*^reg_type^:^l1^

# tensorflow build command with cpu flags
# bazel build -c opt --copt=-mavx --copt=-mavx2 --copt=-mfma --copt=-mfpmath=both --copt=-msse4.2 -k //tensorflow/tools/pip_package:build_pip_package

# hyper parameters
params = {
    'id': 1, # unique id of experiment, if existing it will automatically choose next index

    'cpu_threads': 16, # low dim mode require up to 0.5GB RAM,
    'cpu': 1,
    'gpu': 0,

    'nn_type': 'relu', #lstm, gru, relu, elu, selu, leakyrelu
    'hidden_units': 512, #512
    'is_batch_norm': False,
    'dropout': 0.0, # 0.0 - 1.0 in Q-network produces bad results
    'mini_batch_size': 32,

    'frames_back': 5, # 5
    'lr': 3e-5, # 3e-4
    'reg': 0, # 1e-2,
    'reg_type': 'l2', #l2 regularization algorithm of weights kernels
    'opt': 'adam', # optimizer algorithm type
    'loss_func': 'mse', # mse, mae, medse

    'rand_seed': 0, # serialized rand seeds, 0 = new seed, necessary to make comparisons
    'const_rand_pool': True, # single random seed pool in order to make results reproducible

    'is_memory_unbiased': True, # if game mechanics do not change not very important to use, slows down learning (without bias lower LR needed)
    'frames_in_replay_memory': 5e5,

    'frames_before_start_training': 5e4, #5e4
    'frames_per_epoch': 1e5, #1e5

    'bellman_gamma': 0.99,

    'utility_function': 'dqn', # ddqn_target, mdqn, dqn
    'mdqn_avg': 'average', # average, median, min, max
    'mdqn_models': 2, # can be as Triple DQN or more

    'dueling_network': False, #Dueling Network Architectures for Deep Reinforcement Learning, Deep Mind, 2016
    'is_cumulative_reward': True, # Available only in offline mode to boost performance

    'architecture': '1_to_n', # 1_to_n, n_to_n

    'frames_update_target_network': 5e4, # 5e4 when target_network_alpha > 0.0 after this number of drames weights will be updated in target network
    'target_network_alpha': 1.0, # amount by which to update target network 1.0 => Copy temp network, 0.0 => Do not use target network

    'frames_max_per_episode': 1e4, # 1e4 at 30fps around 5min
    'game': 'raycastmaze',
    # Games with static low dim: flappybird, catcher, pong, pixelcopter, puckworld (No terminal state), snake (Dynamic low dim), waterworld (Dynamic low dim)
    # Visual games: doom, raycastmaze, monsterkong

    'doom_scenario': 'take_cover', # take_cover, basic, deadly_corridor, health_gathering

    'is_train_from_pixels': False, # use high dimensionality state or low dimensionality state (just relevant information)
    'is_grayscale_pixels': False, # in some games color is very important for example raycastmaze red doors, useful for games where shapes are important
    'is_rg_pixels': False, #RGB to RG useful for 3d maze to save memory resources
    'is_use_tiny_convnet': False,
    'is_use_convnet_ext': False,
    'is_state_as_difference': False, # difference between previous state, important when using higher dimension states

    'grad_clip': 0.0, #0.0 L2 clipnorm DQN gradient clipping when used on many environments to avoid grad explosion. Norm clipping to norm 10. Dueling Network Architectures for Deep Reinforcement Learning

    'experience_type': 'rank', # rank, proportional, skewed, random

    'is_load_model': False, # load model by id otherwise create new
    'is_save_experience': False, # experience takes a lot of memory
    'model_prefix': 'last', # last or best (best is serialized when model has highest average rewards per episode - might be misleading)

    'is_save_qmap': False,  # generate image of Q values map for current model
    'qmap_min_score': 0,  # at what score generate sample Q map
    'qmap_epoch_interval': 5, # 0 = do not wait epoch interval, but produce qmap & exit, otherwise generate qmap at specific interval and continue

    'is_playback': False, # playback mode for visualization

    'is_generate_human_samples': False, # seed samples by human, not implemented

    'is_use_previous_action_in_state': False,

    'reward_extra_per_frame': 1e-5, # 1e-5, #small extra revard to stimulate survival, for other games it is not desirable at all like maze
    'reward_terminal_state': -1e3, # huge negative terminal reward for sparse positive reward environments
    'is_terminal_state_negative': False, # should add negative reward to terminal state no matter if terminal state has negative reward or not

    'is_curriculum_learning': False, # for more complex games advisable to get results faster (supported games raycastmaze)
    'is_add_noop_action': True, # allow game environment to have action=None when agent is doing nothing in current frame, way to reduce dimensionality for some games where extra movement does not matter
    'actions_to_ignore': '', # reduce dimensionality of problem by removing unnecessary actions (sperated by symbol |)

    'epsilon_explore_start': 1e-3, # 1e-3 e-greedy percentage of actions taken at random at beginning of learning
    'epsilon_explore_end': 1e-6, # e-greedy at end of learning
    'is_e_greedy': True, # e-greedy or just greedy without exploration
    'is_prob_greedy': False, # get policy using weighted probability instead of np.argmax

    'e_stuck_explore_start': 0.5,
    'e_stuck_explore_end': 1e-3,
    'is_e_stuck': False, # e-greedy policy when player does the same action all the time, important for games like maze, pong

    'is_sarsa': False,

    'is_online': False, # LR - sensitive, online or offline learning, Online learning is much longer, because for every frame it requires batch sample whereas offline sample batches at end only by amount required TODO research direction - compare
    'is_offline_pre_batch': False, # False = update batch experience while iterating through latest data, otherwise presample before learning
    'online_interval': 32, # after every frame or batch (more efficient)

    'is_save_model_plot': False, # save graph of model as png (needed pip install pydot==1.1.0)
    'is_verbose': False, # log evey number of episode

    'results_db': 'default', # name of CSV to store complete results of model testing (useful for grid search of hyperparamters)

    'nb_epochs_end_of_learning': 100, # planned end of learning (needed for weight decay to mitigate experience bias) + e-greedy decay
    'max_nb_epochs': 200, # 100, # when to stop learning, useful for hyperparams optimization

    'evaluation_stride': 10, # 10 # number of epochs taken in account for evaluation report in spreadsheet

    'is_blowup_detection': True, # Avoid wasting resources
    'blowup_loss_plus_order': 2, # Orders of magnitude change in loss that indicate blowup
    'blowup_loss_minus_order': 2, # if orders of magintude change in positive direction and score worst or same then also consider blowup
    'blowup_loss_minus_epochs': 10,
    'blowup_score_const_epochs': 20, #if 20 epochs avg score same algorithm is dead

    'comment': '', # meta data
    'repeat_id': 0, # meta data

    'curriculum_raycastmaze_size_start': 6,
    'curriculum_raycastmaze_size_end': 10,
    'curriculum_raycastmaze_dist_start': 2,
    'curriculum_raycastmaze_dist_end': 20
}

# low dim mode
qmaps_supported_games_low_dim = [
    'flappybird',
    'catcher',
    'pong'
]

# pixel representations
qmaps_supported_games_high_dim = [
    'raycastmaze'
]

# defined later
# needed as dictionary in order to serialize when model is loaded
variables = {
    'epsilon_explore': 0,
    'episodes_total': 0,
    'episodes_epoch': 0,

    'frames_total': 0,
    'frames_epoch': 0,
    'frames_target_network': 0,
    'frames_online': 0,
    'epochs': 0,
    'epoch_is_qmap_generated': False,
    'sarsa_last_action_set': None,

    'epoch_rewards': [],
    'epoch_scores': [],

    'best_epoch_score_avg': -float('Inf'),
    'best_epoch_score_max': -float('Inf'),
    'best_epoch_score_median': -float('Inf'),
    'best_epoch_score_min': float('Inf'),
    'best_epoch_loss': -float('Inf'),

    'epoch_score_avg': 0,
    'epoch_score_max': 0,
    'epoch_score_min': 0,
    'epoch_score_median': 0,

    'epoch_time_started': 0,
    'time_total': 0,

    'all_epoch_loss': [],
    'all_epoch_avg_scores': [],
    'all_epoch_max_scores': [],
    'all_epoch_min_scores': [],
    'all_epoch_median_scores': [],

    'is_blownup': False,

    'curriculum_raycastmaze_size': 0,
    'curriculum_raycastmaze_dist': 0
}


params, params_argv = param_utils.load_params(params)
params['id_request'] = params['id']

if params['is_load_model']:
        params_requested = deepcopy(params)
        # restore params exactly as they were during experiment
        params = np.load('./models/params-{}.npy'.format(params['id'])).item()
        params_dont_touch = []

        # leave intact following params
        params_dont_touch = ['is_load_model',
                             'is_save_qmap',
                             'max_nb_epochs',
                             'qmap_min_score',
                             'model_prefix',
                             'is_save_model_plot',
                             'qmap_epoch_interval',
                             'is_generate_human_samples',
                             #'is_e_stuck',
                             #'e_stuck_explore_end',
                             'cpu_threads',
                             'cpu',
                             'gpu',
                             'is_playback',
                             'is_verbose']

        # legacy support, missing keys
        for key in params_requested:
            if not key in params:
                params_dont_touch.append(key)

        for key in params_dont_touch:
            params[key] = params_requested[key]

# These games have only high dimensionality representations
if params['game'] in ['doom', 'raycastmaze', 'monsterkong']:
    params['is_train_from_pixels'] = True


# initialize seed and store it
# Very important to set same random seed for all experiments that we want to compare
np.random.seed(None)
max_seed =  2**32 - 1

if params['rand_seed']:
    np.random.seed(params['rand_seed'])
    print( 'restored random seed:', params['rand_seed'])
else:
    seed = np.random.randint(max_seed)
    np.random.seed(seed)
    params['rand_seed'] = seed

# following based on numpy seed
import tensorflow as tf
tf.set_random_seed(np.random.randint(max_seed))
random.seed(np.random.randint(max_seed)) # for experience replay


# keras must be imported after random seed
from keras.models import Sequential, model_from_json
from keras.layers import Concatenate, Merge, Lambda, Dense, Activation, SimpleRNN, LSTM, Flatten, BatchNormalization, Conv2D, MaxPooling2D, Reshape, GRU, Dropout
from keras.layers import Embedding
from keras.engine import Input
from keras.optimizers import RMSprop, SGD, Adam
from keras.models import Model
from keras.layers.wrappers import TimeDistributed
from keras.regularizers import l1, l2
from keras.initializers import glorot_uniform, orthogonal
from keras.layers.advanced_activations import LeakyReLU, PReLU
import keras.backend as K
import keras.models
from keras.utils import plot_model
import utils #include keras, must be included after random seed

# PyGame-Learning-Environment
import ple
import pygame.display

# prioritized-experience-replay
import proportional
import rank_based

if not params['is_load_model']:
    path_log = './logs/{}.log'.format(params['id_request'])
    if os.path.exists(path_log):
        is_log_in_use = False
        with open(path_log, 'r') as fp:
            if (len(fp.readlines()) > 2):
                is_log_in_use = True
            fp.close()

        if is_log_in_use:
            # check against id collision
            existing_logs = os.listdir('./logs/')
            for candidate in existing_logs:
                results = re.search('([0-9]+).log', candidate)
                if results is not None and len(results.groups()):
                    id_last = int(results.groups()[0])
                    if id_last >= params['id']:
                        params['id'] = id_last + 1
        if params['id'] < 0:
            params['id'] = 1

    # save params
    if not params['is_playback']:
        np.save('./models/params-{}.npy'.format(params['id']), params)


if os.environ['KERAS_BACKEND'] == 'tensorflow':

    random_pool_threads = 0 # automatic
    if params['const_rand_pool']:
        random_pool_threads = 1

    config= K.tf.ConfigProto( allow_soft_placement=True, inter_op_parallelism_threads=random_pool_threads, intra_op_parallelism_threads=params['cpu_threads'],
                            device_count={'CPU': params['cpu'], 'GPU': params['gpu']})

    sess = K.tf.Session(config=config)
    K.set_session(sess)

# init files for holding results
param_utils.init_log('./logs/{}.log'.format(params['id']))
loss_csv = open('./results/loss-{}.csv'.format(params['id']), 'a')

curriculum_str = ''
if params['is_curriculum_learning']:
    if params['game'] == 'raycastmaze':
        curriculum_str = ';'.join([
            'c_size',
            'c_dist'
        ])

if not params['is_playback']:
    loss_csv.write(
        ';'.join(['epochs', 'episodes', 'loss',
                  'epoch_reward_max', 'epoch_reward_min',
                  'epoch_reward_median', 'epoch_reward_avg',
                  'epoch_score_max',
                  'epoch_score_min', 'epoch_score_median',
                  'epoch_score_avg', curriculum_str, 'epoch_time']) + '\n')

logging.info('params: {}'.format(param_utils.format_params(params)))

if not params['is_playback']:
    params_csv = open('./results/params-{}.csv'.format(params['id']), 'w')
    for key in params.keys():
        logging.info('{}: {}'.format(key, params[key]))
        params_csv.write('{};{}\n'.format(key, params[key]))
    params_csv.flush()

if not params['is_playback'] and params['game'] != 'doom':
    # disable sounds
    os.environ['SDL_AUDIODRIVER'] = "waveout"
    # disable visuals - headless
    os.environ["SDL_VIDEODRIVER"] = "dummy"

# exploration params
variables['epsilon_explore'] = params['epsilon_explore_start'] if params['is_e_greedy'] else 0
variables['epsilon_e_stuck'] = params['e_stuck_explore_start'] if params['is_e_stuck'] else 0

epsilon_explore_decay_per_episode = (params['epsilon_explore_start'] - params['epsilon_explore_end']) / float(params['frames_per_epoch'] * params['nb_epochs_end_of_learning'])
epsilon_stuck_decay_per_episode = (params['e_stuck_explore_start'] - params['e_stuck_explore_end']) / float(params['frames_per_epoch'] * params['nb_epochs_end_of_learning'])


# define PLE game
game = None
env = None

if params['game'] == 'flappybird':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/flappybird.html

    # player y position.
    # players velocity.
    # next pipe distance to player
    # next pipe top y position
    # next pipe bottom y position
    # next next pipe distance to player
    # next next pipe top y position
    # next next pipe bottom y position

    game = ple.games.flappybird.FlappyBird()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_flappy_bird_low_dimensions_state)

elif params['game'] == 'doom':
    import ple.games.doom
    game = ple.games.Doom(scenario=params['doom_scenario'])

    # simultaneous action set
    # MOVE_LEFT, MOVE_RIGHT, ATTACK
    # 5 more combinations are naturally possible but only 3 are included for transparency when watching.
    # actions = [[True, False, False], [False, True, False], [False, False, True]]

    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'])

elif params['game'] == 'catcher':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/catcher.html
    # 64x64
    # reward for a catch
    # 0 fruits x position
    # 1 players velocity
    # 2 player x position
    # 3 fruits y position
    game = ple.games.Catcher()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_game_low_dimensions_state)

elif params['game'] == 'pong':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/pong.html
    # 64x48
    # reward when score in opponents empty space
    # 0 ball x position.
    # 1 ball y position.
    # 2 ball y velocity.
    # 3 ball x velocity.
    # 4 players velocity.
    # 5 cpu y position.
    # 6 player y position.

    game = ple.games.Pong(cpu_speed_ratio=0.6, players_speed_ratio=0.4, MAX_SCORE=1)
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_game_low_dimensions_state)

elif params['game'] == 'puckworld':
    # No terminal state, will play till max frame per episode
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/puckworld.html
    # player x position.
    # player y position.
    # players x velocity.
    # players y velocity.
    # good creep x position.
    # good creep y position.
    # bad creep x position.
    # bad creep y position.
    game = ple.games.PuckWorld()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_game_low_dimensions_state)

elif params['game'] == 'raycastmaze':
    # TODO no bad terminal state, needs research, map +1 size
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/raycastmaze.html
    # NO LOW Dimension representation

    if params['is_curriculum_learning']:
        variables['curriculum_raycastmaze_size'] = params['curriculum_raycastmaze_size_start']
        variables['curriculum_raycastmaze_dist'] = params['curriculum_raycastmaze_dist_start']
    else:
        variables['curriculum_raycastmaze_size'] = params['curriculum_raycastmaze_size_end']
        variables['curriculum_raycastmaze_dist'] = params['curriculum_raycastmaze_dist_end']

    game = ple.games.RaycastMaze(width=48, height=48, map_size=variables['curriculum_raycastmaze_size'], init_pos_distance_to_target=variables['curriculum_raycastmaze_dist'])
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'])

elif params['game'] == 'snake':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/snake.html
    # snake head x position.
    # snake head y position.
    # food x position.
    # food y position.
    # distance from head to each snake segment.
    game = ple.games.Snake()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_snake_low_dimensions_state)

elif params['game'] == 'waterworld':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/waterworld.html
    # player x position.
    # player y position.
    # player x velocity.
    # player y velocity.
    # player distance to each creep
    game = ple.games.WaterWorld()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_waterworld_low_dimensions_state)

elif params['game'] == 'pixelcopter':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/pixelcopter.html
    # player y position.
    # player velocity.
    # player distance to floor.
    # player distance to ceiling.
    # next block x distance to player.
    # next blocks top y location,
    # next blocks bottom y location.
    game = ple.games.Pixelcopter()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'], state_preprocessor=utils.process_game_low_dimensions_state)

elif params['game'] == 'monsterkong':
    # http://pygame-learning-environment.readthedocs.io/en/latest/user/games/monsterkong.html

    # very difficult game
    # No LOW dimension state
    game = ple.games.MonsterKong()
    env = ple.PLE(game, add_noop_action=params['is_add_noop_action'], display_screen=params['is_playback'])

env.init()

low_dimensions_state = None
if not params['is_train_from_pixels']:
    low_dimensions_state = env.getGameStateDims()[0]
    if params['is_use_previous_action_in_state']:
        low_dimensions_state += 1

high_dimensions_width, high_dimensions_height = env.getScreenDims()
high_dimensions_channels = 3 # could be also just grayscale
if params['is_grayscale_pixels']:
    high_dimensions_channels = 1
elif params['is_rg_pixels']:
    high_dimensions_channels = 2

actions_available = env.getActionSet()
actions_to_ignore = params['actions_to_ignore'].split('|')
actions_to_ignore = [it.strip() for it in actions_to_ignore]
# names of actions
keyed_actions = env.game.actions
for key in keyed_actions:
    if isinstance(key, str):
        value = keyed_actions[key]
        if key in actions_to_ignore: # remove some actions to reduce dimensionality
            actions_available.remove(value)
dimensions_actions = len(actions_available)

def reshape_to_single_timestep_state(x):
    x_input = None
    if not params['is_train_from_pixels']:
        x_input = np.reshape(np.array(x), (1, params['frames_back'], low_dimensions_state))
    else:
        x_input = np.reshape(np.array(x), (1, params['frames_back'], high_dimensions_width, high_dimensions_height, high_dimensions_channels))
    return x_input


# building Q network model
logging.info('model build started')

model_arch_group = []
model_arch_group_count = 1 if params['architecture'] == '1_to_n' else dimensions_actions

# keras custom objects
custom_objects = {
    'leakyrelu': utils.leakyrelu }

model_temps_count = 1
if params['utility_function'] == 'mdqn':
    model_temps_count = params['mdqn_models']


if params['is_load_model']:

    # legacy support
    indexes = ['']
    for idx_arch in range(model_arch_group_count):
        indexes.append(str(idx_arch) + '-')

    for idx_arch in indexes:
        filename = './models/{}_model_target-{}{}.h5'.format(params['model_prefix'], idx_arch, params['id'])
        if os.path.exists(filename):
            model_target = keras.models.load_model(filename, custom_objects=custom_objects)
            model_temps = []
            for i in range(model_temps_count):
                model_temps.append(keras.models.load_model('./models/{}_model_temp_{}-{}{}.h5'.format(params['model_prefix'], i, idx_arch, params['id']), custom_objects=custom_objects))

            model_arch_group.append({
                'model_target': model_target,
                'model_temps': model_temps
            })

    #restore variables
    variables_before = variables
    variables = np.load('./models/{}_variables-{}.npy'.format(params['model_prefix'], params['id'])).item()

    # legacy, missing keys
    for key in variables_before.keys():
        if not key in variables:
            variables[key] = variables_before[key]

    variables['epoch_is_qmap_generated'] = False
    variables['is_blownup'] = False

    logging.info('model loaded')

    if not params['is_playback']:
        if variables['epochs'] >= params['max_nb_epochs']:
            logging.info('learning already finished')
            exit()


else:

    for idx_arch in range(model_arch_group_count):
        model_target = Sequential()
        model_temps = []
        for _ in range(model_temps_count):
            model_temps.append(Sequential())

        if params['is_train_from_pixels']:
            if params['is_use_convnet_ext']:
                model_target.add(TimeDistributed(Conv2D(32, (7, 7), strides=(2, 2), activation='relu', padding='same',
                                                 input_shape=(high_dimensions_width, high_dimensions_height,
                                                              high_dimensions_channels)), input_shape=(
                    params['frames_back'], high_dimensions_width, high_dimensions_height, high_dimensions_channels)))
                model_target.add(TimeDistributed(Conv2D(32, (3, 3), kernel_initializer="he_normal", activation='relu')))
                model_target.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

                model_target.add(TimeDistributed(Conv2D(64, (3, 3), padding='same', activation='relu')))
                model_target.add(TimeDistributed(Conv2D(64, (3, 3), padding='same', activation='relu')))
                model_target.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

                model_target.add(TimeDistributed(Conv2D(128, (3, 3), padding='same', activation='relu')))
                model_target.add(TimeDistributed(Conv2D(128, (3, 3), padding='same', activation='relu')))
                model_target.add(TimeDistributed(MaxPooling2D((2, 2), strides=(2, 2))))

                model_target.add(TimeDistributed(Flatten()))
            elif params['is_use_tiny_convnet']:
                model_target.add(TimeDistributed(
                    Conv2D(16, (4, 4), kernel_initializer=glorot_uniform(), padding='same', activation='relu',
                           input_shape=(high_dimensions_width, high_dimensions_height,
                                        high_dimensions_channels)), input_shape=(
                    params['frames_back'], high_dimensions_width, high_dimensions_height, high_dimensions_channels)))
                model_target.add(TimeDistributed(MaxPooling2D((2, 2))))
                model_target.add(TimeDistributed(
                    Conv2D(32, (2, 2), kernel_initializer=glorot_uniform(), padding='same', activation='relu')))
                model_target.add(TimeDistributed(MaxPooling2D((2, 2))))
                model_target.add(TimeDistributed(
                    Conv2D(64, (2, 2), kernel_initializer=glorot_uniform(), padding='same', activation='relu')))
                model_target.add(TimeDistributed(Reshape((-1,))))
            else:
                model_target.add(TimeDistributed(
                    Conv2D(32, (8, 8), strides=(4, 4), kernel_initializer=glorot_uniform(), padding='same',
                           activation='relu',
                           input_shape=(high_dimensions_width, high_dimensions_height, high_dimensions_channels)),
                    input_shape=(
                    params['frames_back'], high_dimensions_width, high_dimensions_height, high_dimensions_channels)))
                model_target.add(TimeDistributed(
                    Conv2D(64, (4, 4), strides=(2, 2), kernel_initializer=glorot_uniform(), activation='relu')))
                model_target.add(TimeDistributed(
                    Conv2D(64, (3, 3), kernel_initializer=glorot_uniform(), padding='same', activation='relu')))
                model_target.add(TimeDistributed(Reshape((-1,))))


        if params['nn_type'] == 'lstm':
            if params['is_train_from_pixels']:
                model_target.add(utils.process_layer(LSTM(params['hidden_units'], kernel_initializer=glorot_uniform(), recurrent_initializer=orthogonal(), return_sequences=True, dropout=params['dropout'], recurrent_dropout=params['dropout']), params))
            else:
                model_target.add(utils.process_layer(LSTM(params['hidden_units'], kernel_initializer=glorot_uniform(), recurrent_initializer=orthogonal(), input_shape=(params['frames_back'], low_dimensions_state), return_sequences=True, dropout=params['dropout'], recurrent_dropout=params['dropout']), params))
            # dropout implemented as a param in prev. line
            if params['is_batch_norm']:
                model_target.add(BatchNormalization())
            model_target.add(utils.process_layer(LSTM(params['hidden_units']), params))
        elif params['nn_type'] == 'gru':
            if params['is_train_from_pixels']:
                model_target.add(utils.process_layer(GRU(params['hidden_units'], kernel_initializer=glorot_uniform(), recurrent_initializer=orthogonal(), return_sequences=True, dropout=params['dropout'], recurrent_dropout=params['dropout']), params))
            else:
                model_target.add(utils.process_layer(GRU(params['hidden_units'], kernel_initializer=glorot_uniform(), recurrent_initializer=orthogonal(), input_shape=(params['frames_back'], low_dimensions_state), return_sequences=True, dropout=params['dropout'], recurrent_dropout=params['dropout']), params))
            # dropout implemented as a param in prev. line
            if params['is_batch_norm']:
                model_target.add(BatchNormalization())
            model_target.add(utils.process_layer(GRU(params['hidden_units']), params))
        else:

            if params['is_train_from_pixels']:
                model_target.add(TimeDistributed(utils.process_layer(Dense(params['hidden_units'],
                                                                           kernel_initializer=glorot_uniform(),
                                                                           name='DenseMain1'), params, is_load_activation = True)))
            else:
                model_target.add(TimeDistributed(utils.process_layer(Dense(params['hidden_units'],
                                                                           kernel_initializer=glorot_uniform(),
                                                                           name='DenseMain1'), params, is_load_activation = True),
                                                 input_shape=(params['frames_back'], low_dimensions_state)))

            model_target.add(Flatten())
            if params['dropout'] > 0.0:
                model_target.add(Dropout(rate=params['dropout']))
            if params['is_batch_norm']:
                model_target.add(BatchNormalization())

            model_target.add(utils.process_layer(Dense(params['hidden_units'], name='DenseMain2',kernel_initializer=glorot_uniform()), params, is_load_activation = True))

        if params['dueling_network']:
            # As described in https://arxiv.org/abs/1511.06581

            if(len(model_target.layers[-1].output_shape) == 3):
                model_target.add(Flatten())

            model_left = Sequential()
            model_left.add(model_target.layers[-1])
            model_left.add(utils.process_layer(Dense(params['hidden_units'], name='fc1', input_shape=model_target.layers[-1].output_shape, kernel_initializer=glorot_uniform()), params, is_load_activation=True))
            model_left.add(utils.process_layer(Dense(dimensions_actions, name='advantage', kernel_initializer=glorot_uniform(), activation=None), params) )

            model_right = Sequential()
            model_right.add(model_target.layers[-1])
            model_right.add(utils.process_layer(Dense(params['hidden_units'], name='fc2', input_shape=model_target.layers[-1].output_shape, kernel_initializer=glorot_uniform()), params, is_load_activation=True))
            model_right.add(utils.process_layer(Dense(1, name='value', kernel_initializer=glorot_uniform(), activation=None), params))

            action_count_output = dimensions_actions
            if params['architecture'] == 'n_to_n':
                action_counpipt_output = 1

            merged_net = Lambda(function = lambda x : x[0] - K.mean(x[0]) + x[1], output_shape=(action_count_output,))([model_left.layers[-1].output, model_right.layers[-1].output])

            model_target = Model(inputs=model_target.layers[0].input, outputs=merged_net)
        else:
            if params['architecture'] == 'n_to_n':
                model_target.add(utils.process_layer(Dense(1, kernel_initializer=glorot_uniform(), name='DenseLinear'), params))
            else:
                model_target.add(utils.process_layer(Dense(dimensions_actions,kernel_initializer=glorot_uniform(), name='DenseLinear'), params))

        if params['is_save_model_plot']:
            plot_model(model_target, show_shapes=True, to_file='./results/plot_model_{}.png'.format(params['id']))


        loss_func = params['loss_func']

        if loss_func == 'medse':
            def medse(y_true, y_pred):
                v = K.reshape(K.square(y_pred - y_true), [-1])
                median = tf.contrib.distributions.percentile(v, 50.0)
                return median
            loss_func = medse

        model_target.compile(loss=loss_func, optimizer='sgd') #target not used for learning

        for i in range(model_temps_count):
            optimizer = None
            if(params['opt'] == 'sgd'):
                optimizer = SGD(lr=params['lr'], clipnorm=params['grad_clip'])
            elif(params['opt'] == 'rmsprop'):
                optimizer = RMSprop(lr=params['lr'], clipnorm=params['grad_clip'])
            elif (params['opt'] == 'adam'):
                optimizer = Adam(lr=params['lr'], clipnorm=params['grad_clip'])

            model_temps[i] = keras.models.model_from_json(model_target.to_json(), custom_objects=custom_objects)
            model_temps[i].compile(loss=loss_func, optimizer=optimizer)
            model_temps[i].set_weights(model_target.get_weights())

        model_arch_group.append({
            'model_target': model_target,
            'model_temps': model_temps
        })

    logging.info('model build finished')

x = None
if not params['is_train_from_pixels']:
    x = np.zeros((params['frames_back'], low_dimensions_state)).tolist()
else:
    x = np.zeros((params['frames_back'], high_dimensions_width, high_dimensions_height, high_dimensions_channels)).tolist()

replay_params = {'size': params['frames_in_replay_memory'],
    'learn_start': params['frames_before_start_training'], #Min amount of frames before training (for calculation of Beta)
    # steps = episodes
    'total_steps': params['nb_epochs_end_of_learning'] * params['frames_per_epoch'], #For calculation of Beta http://www.evernote.com/l/ACnDUVK3ShVEO7fDm38joUGNhDik3fFaB5o/
    'batch_size': params['mini_batch_size']}


if params['experience_type'] == 'rank':
    experience = rank_based.Experience(replay_params)
elif params['experience_type'] == 'proportional':
    experience = proportional.Experience(replay_params)
else:
    assert False, "experience_type: '%s' is not implemented" % params['experience_type']

if params['is_load_model'] and os.path.exists('./models/{}_experience-{}.npy'.format(params['model_prefix'], params['id'])):
    experience.load('./models/{}_experience-{}.npy'.format(params['model_prefix'], params['id']))
    logging.info('experience loaded')

# Temporary variables
prev_state = None
frame = None
prev_action = -1
variables['epoch_time_started'] = int(time.time())
loss = float('Inf')
losses = []

experience_episode = []

# function to choose action - needed in order to implement SARSA
# choose action with some decaying randomness
#@profile
def get_action(q_values, prev_action = -1):
    global actions_available, \
        variables, \
        params

    action_index = 0
    if params['is_prob_greedy']:
        q_values_mod = q_values.tolist()
        q_values_mod.sort()
        q_minimal_value = abs(q_values_mod[0])

        for idx_qvm in xrange(len(q_values_mod)):
            q_values_mod[idx_qvm] += q_minimal_value

        sum_probs = np.sum(q_values_mod)
        act_prob = np.random.random() * sum_probs

        sum_probs = 0.0
        for idx_action_prob, q_value in enumerate(q_values_mod):

            sum_probs += q_value * 0.5
            if idx_action_prob + 1 < len(q_values_mod):
                sum_probs += q_values_mod[idx_action_prob + 1] * 0.5

            if act_prob <= sum_probs:
                action_index = idx_action_prob
                break
    else:
        action_index = np.argmax(q_values)
    action_candidate = actions_available[action_index]

    # e-greedy
    if params['is_generate_human_samples'] or params['is_e_greedy'] or (params['is_e_stuck'] and action_candidate == prev_action):
        rand_val = random.random()
        if params['is_generate_human_samples'] or \
            (params['is_e_greedy'] and rand_val < variables['epsilon_explore']) or \
            (params['is_e_stuck'] and action_candidate == prev_action and rand_val < variables['epsilon_e_stuck']):

            if params['is_playback'] and not params['is_generate_human_samples']:
                logging.info('random action')

            action_index = 1
            if params['is_generate_human_samples']:
                # human controlled action
                keys = pygame.key.get_pressed()
                if params['game'] == 'flappybird':
                    if keys[pygame.K_SPACE]:
                        action_index = 0
                if params['game'] == 'pong':
                    action_index = len(actions_available) - 1
                    if keys[pygame.K_UP]:
                        action_index = 1
                    elif keys[pygame.K_DOWN]:
                        action_index = 0
                if params['game'] == 'raycastmaze':
                    action_index = len(actions_available) - 1
                    if keys[pygame.K_UP]:
                        action_index = 0
                    elif keys[pygame.K_LEFT]:
                        action_index = 3
                    elif keys[pygame.K_DOWN]:
                        action_index = 2
                    elif keys[pygame.K_RIGHT]:
                        action_index = 1

            else:
                # NPC random action
                action_index = int(random.random() * dimensions_actions)

                if params['is_e_stuck'] and rand_val < variables['epsilon_e_stuck'] and action_candidate == prev_action:
                    # avoid randomly choosing same action for e_stuck mode
                    action_index += 1
                    if action_index >= len(actions_available):
                        action_index = 0

            # Not important what value is set to Q when rand index used, because it will be overrided with true R value when learning
            if not np.argmax(q_values) == action_index:
                q_values[action_index] = float(np.max(q_values) + 1.0)

            # e-greedy policy
    if params['is_verbose']:
        print(action_index, q_values, time.time())

    return actions_available[action_index], q_values

def get_qvalues(x_input):
    global params, dimensions_actions, model_arch_group, env
    q_values = None

    if params['target_network_alpha'] > 0.0:
        # MDQN also can be merged into target network (see model update part)
        if params['architecture'] == 'n_to_n':
            q_values = []
            # every action has its own model
            for idx_arch in range(len(model_arch_group)):
                q_value = np.reshape(model_arch_group[idx_arch]['model_target'].predict(x_input, batch_size=1), (1,))
                q_values.append(q_value)
            q_values = np.array(q_values)
        else:
            model_target = model_arch_group[0]['model_target']
            raw_q_values = model_target.predict(x_input, batch_size=1)
            q_values = np.reshape(raw_q_values, (dimensions_actions,))

            keys = pygame.key.get_pressed()
            layer_idx = 0
            if keys[pygame.K_2]:
                layer_idx = 2
            elif keys[pygame.K_3]:
                layer_idx = 3
            elif keys[pygame.K_4]:
                layer_idx = 4
            elif keys[pygame.K_5]:
                layer_idx = 5
            elif keys[pygame.K_6]:
                layer_idx = 6
            elif keys[pygame.K_7]:
                layer_idx = 7
            elif keys[pygame.K_8]:
                layer_idx = 8
            elif keys[pygame.K_9]:
                layer_idx = 9

            if layer_idx != 0:
                # generate grad cam attention map by pressing 0 key (snapshot of screen)
                utils.generate_grad_cam_map(env, model_target, x_input, raw_q_values, params, variables, layer_idx)

    else: # target_network_alpha == 0.0
        # MDQN use avg over decoupled models
        # in case of DQN only one model_temps

        q_values_cumulative = []

        if params['architecture'] == 'n_to_n':
            # every action has its own model
            for idx_arch in range(len(model_arch_group)):
                model_temps = model_arch_group[idx_arch]['model_temps']
                for idx_each, model_each in enumerate(model_temps):
                    q_value = np.reshape(model_each.predict(x_input, batch_size=1), (1,))
                    if idx_arch == 0:
                        q_values_cumulative.append([])
                    q_values_cumulative[idx_each].append(q_value)
        else:
            model_temps = model_arch_group[0]['model_temps']
            for model_each in model_temps:
                q_values_cumulative.append(np.reshape(model_each.predict(x_input, batch_size=1), (dimensions_actions,)))

        if params['mdqn_avg'] == 'average':  # Original paper used avg over 2 decoupled models
            q_values = np.average(np.array(q_values_cumulative), axis=0)
        elif params['mdqn_avg'] == 'median':
            q_values = np.median(np.array(q_values_cumulative), axis=0)
        elif params['mdqn_avg'] == 'min':
            q_values = np.min(np.array(q_values_cumulative), axis=0)
        elif params['mdqn_avg'] == 'max':
            q_values = np.max(np.array(q_values_cumulative), axis=0)

    return q_values

#@profile
def get_action_from_state():
    global prev_action, variables, params, x, env, prev_state_orig, frame
    state = []

    if not params['is_train_from_pixels']:
        # low dimension representation of game
        state = env.getGameState().tolist()

        if params['is_use_previous_action_in_state']:
            state.append(prev_action + 1)  # zero index = NAN
    else:
        # high dimension representation of game (from visible pixels)
        if params['is_grayscale_pixels']:
            # numpy array with the shape (width, height)
            state = np.reshape(env.getScreenGrayscale(), (high_dimensions_width, high_dimensions_height, 1))
        elif params['is_rg_pixels']:
            # numpy array with the shape (width, height, 2)
            state = utils.convertScreenRG(env.getScreenRGB())
        else:
            # numpy array with the shape (width, height, 3)
            state = np.array(env.getScreenRGB())

        # normalize data to store input values in network from -1.0 to 1.0
        state = (state - 125.0) / 255.0

    state_orig = np.array(state)

    if params['is_state_as_difference']:
        if prev_state_orig is None:
            state = np.zeros_like(state)
        else:
            state = state - prev_state_orig

    x.append(state)
    x = x[-params['frames_back']:]

    # forecast Q values
    x_input = reshape_to_single_timestep_state(x)

    q_values = get_qvalues(x_input)

    action, q_values = get_action(q_values, prev_action)

    return state, state_orig, action, q_values

#@profile
def main():
    global prev_state, \
        prev_state_orig, \
        frame, \
        variables, \
        loss, \
        losses, \
        x, \
        prev_action, \
        experience, \
        params, \
        model_arch_group, \
        model_temps_count, \
        env, \
        game, \
        experience_episode, \
        epsilon_stuck_decay_per_episode, \
        epsilon_explore_decay_per_episode

    # Avoid loss explosion NaN, iterate till max defined epochs or allow playback mode
    while (variables['epochs'] < params['max_nb_epochs'] and not variables['is_blownup'] and not math.isnan(loss)) or params['is_playback']:

        # Add task id to stop.txt in order to stop execution graciously
        is_force_stop = False

        if params['is_verbose'] and variables['episodes_total'] > 0 and variables['frames_total'] >= params['frames_before_start_training']:
            logging.info('episode: {} {} {}'.format(variables['episodes_total'], len(experience_episode), variables['epoch_score_max']))

        prev_state = None # with delta/diff between states applied
        prev_state_orig = None # without delta/diff applied
        prev_action = -1
        experience_episode = []
        score_in_episode = 0

        for frame in range(int(params['frames_max_per_episode'])):

            state = None
            action = None
            q_values = None

            variables['frames_total'] += 1
            variables['frames_epoch'] += 1
            variables['frames_target_network'] += 1

            if params['is_sarsa'] and not variables['sarsa_last_action_set'] is None:
                # use SARSA to select action before model update
                state, state_orig, action, q_values = variables['sarsa_last_action_set']
            else:
                # choose action using e-greedy for gameplay
                state, state_orig, action, q_values = get_action_from_state()

            # added small extra reward to encourage moving forward or negative reward to reduce steps of movement
            reward = env.act(action)

            if params['is_verbose']:
                print('reward: {}'.format(reward))

            reward += params['reward_extra_per_frame']

            # store next action before update
            if params['is_sarsa']:
                state_next, state_next_orig, action_next, q_values_next = get_action_from_state()
                variables['sarsa_last_action_set'] = (state_next, state_next_orig, action_next, q_values_next)

            is_game_over = env.game_over()
            keys = pygame.key.get_pressed()
            if frame == int(params['frames_max_per_episode']) - 1 or keys[pygame.K_ESCAPE]:
                is_game_over = True

            # Negative terminal state
            if (params['is_terminal_state_negative'] or reward < 0) and env.game_over():
                reward += params['reward_terminal_state']

                if params['is_verbose']:
                    print('reward: {}'.format(reward))
            else:
                score_in_episode += reward

            if params['is_playback'] and is_game_over:
                logging.info('score {}'.format(score_in_episode))
                if is_game_over:
                    time.sleep(1)

            record = {
                'q': q_values,
                'x_0': utils.copy_x(x),
                'x_1': None,
                'r': reward,
                'action': actions_available.index(action),
                'r_cumulative': 0.0,
                'y_bellman': None
            }
            experience_episode.append(record)


            # store Q maps
            if params['is_save_qmap']:
                if not params['is_train_from_pixels'] and params['game'] in qmaps_supported_games_low_dim or \
                   params['is_train_from_pixels'] and params['game'] in qmaps_supported_games_high_dim:
                    if params['qmap_epoch_interval'] == 0 or \
                        variables['epochs'] % params['qmap_epoch_interval'] == 0 or \
                        variables['epochs'] ==  params['max_nb_epochs'] - 1: #last epoch
                        if not variables['epoch_is_qmap_generated']:
                            if utils.generate_qmap(get_qvalues, params, variables, env, score_in_episode, state, reshape_to_single_timestep_state, x, dimensions_actions):
                                break


            # Even though DQN can be run in online mode - if game requires momentary action bound by FPS so training must be done after episode
            # With PLE it is regulated, but with other frameworks it could not be - this must be taken in consideration
            if not params['is_playback'] and len(experience_episode) > 1 and variables['frames_total'] >= params['frames_before_start_training']:

                if is_game_over or params['is_online']:
                    if is_game_over:
                        # Play backwards experience to store TD errors
                        # Can calculate real cumulative reward
                        experience_episode_rev = list(reversed(experience_episode))
                        next_state = None
                        reward_next = 0
                        reward_total = 0

                        score = 0
                        for t_back in range(0, len(experience_episode_rev)):
                            state = experience_episode_rev[t_back]

                            reward_total = state['r'] + params['bellman_gamma'] * reward_next

                            variables['epoch_rewards'].append(reward_total)
                            reward_next = reward_total

                            # not terminal state or positive terminal state
                            if (t_back > 0 or state['r'] > 0):
                                score += state['r']

                            state['x_1'] = next_state

                            # TODO research direction train by real Q value - why not? real cumulative reward
                            # This available only in offline mode!
                            state['r_cumulative'] = reward_total

                            if not params['is_online']:
                                # Offline mode
                                experience.store(state)

                            next_state = utils.copy_x(state['x_0'])

                        # store statistics of episode
                        variables['epoch_scores'].append(score)

                    if params['is_online']:
                        # Online mode
                        # Cannot calculate real cumulative reward, but only bellman equation
                        state = experience_episode[1]
                        state['x_1'] = utils.copy_x(experience_episode[0]['x_0'])

                        experience.store(state)
                        variables['frames_online'] += 1

                        if is_game_over:
                            state = experience_episode[0]
                            state['x_1'] = None
                            experience.store(state)
                            variables['frames_online'] += 1

                    # learning iteration either offline or with online_interval interval
                    if experience.index >= params['mini_batch_size'] and (not params['is_online'] or variables['frames_online'] >= params['online_interval']):
                        variables['frames_online'] = 0

                        # If offline then we should resample more to cover whole acquired experience
                        sample_iterations = 1
                        if not params['is_online']:
                            sample_iterations = int(math.ceil(len(experience_episode) / float(params['mini_batch_size'])))

                        pre_sampled_experiences = []
                        if params['is_offline_pre_batch']:
                            for _ in range(sample_iterations):
                                batch_experience, batch_w, batch_rank_e_id = experience.sample(global_step=variables['frames_total'])
                                pre_sampled_experiences.append( (batch_experience, batch_w, batch_rank_e_id) )

                        for idx_sample_iterations in range(sample_iterations):

                            batch_experience = None
                            batch_w = None
                            batch_rank_e_id = None

                            if params['is_offline_pre_batch']:
                                batch_experience, batch_w, batch_rank_e_id = pre_sampled_experiences[idx_sample_iterations]
                            else:
                                batch_experience, batch_w, batch_rank_e_id = experience.sample(global_step=variables['frames_total'])

                            optimized_x1_dict = {}  # x_1 results
                            optimized_x0_dict = {}  # x_0 results

                            for idx_arch, model_arch in enumerate(model_arch_group):

                                model_target = model_arch['model_target']
                                model_temps = model_arch['model_temps']

                                model_current = model_temps[0]
                                model_decoupled = model_target # used for DDQN learning

                                if params['utility_function'] == 'mdqn':
                                    # in case of mdqn implementation choose on random between 2 temp models
                                    index_model_current = np.random.randint(0, model_temps_count)
                                    indexes_other = np.arange(0, model_temps_count)
                                    indexes_other = np.delete(indexes_other, index_model_current)
                                    index_model_decoupled = indexes_other[np.random.randint(0, indexes_other.shape[0])]

                                    model_current = model_temps[index_model_current]
                                    model_decoupled = model_temps[index_model_decoupled]

                                model_arch['model_current'] = model_current
                                model_arch['model_decoupled'] = model_decoupled

                                # first calculate q values in batches to optimize for speed
                                optimized_x_batch = []
                                optimized_x_index_batch = []

                                # optimization - batch together x_0 & x_1 forecasts
                                for index_state, state in enumerate(batch_experience):
                                    if not state['x_1'] is None:
                                        x_input = reshape_to_single_timestep_state(state['x_1'])[0]
                                        optimized_x_batch.append(x_input)
                                        optimized_x_index_batch.append((index_state, optimized_x1_dict))
                                        # when skipped it will continue indexing sync, because optimized_x1_dict is dict not list

                                    x_input = reshape_to_single_timestep_state(state['x_0'])[0]
                                    optimized_x_batch.append(x_input)
                                    optimized_x_index_batch.append((index_state, optimized_x0_dict))

                                # optimization
                                optimized_q_batch = None

                                if(params['utility_function'] == 'ddqn_target' or params['utility_function'] == 'mdqn'):
                                    optimized_q_batch = model_decoupled.predict(np.array(optimized_x_batch), batch_size=len(optimized_x_batch))
                                elif(params['utility_function'] == 'dqn'):
                                    optimized_q_batch = model_current.predict(np.array(optimized_x_batch), batch_size=len(optimized_x_batch))

                                # optimization - store results
                                for index_opt, optimized_q in enumerate(optimized_q_batch.tolist()):
                                    index_state, dict_instance = optimized_x_index_batch[index_opt]

                                    if params['architecture'] == 'n_to_n':
                                        q_values = np.reshape(optimized_q, (1,))
                                        if not index_state in dict_instance:
                                            dict_instance[index_state] = np.array([])
                                        dict_instance[index_state] = np.concatenate( (dict_instance[index_state], q_values) )
                                    else:
                                        q_values = np.reshape(optimized_q, (dimensions_actions,))
                                        dict_instance[index_state] = np.array(q_values)

                            for idx_arch, model_arch in enumerate(model_arch_group):
                                model_target = model_arch['model_target']
                                model_temps = model_arch['model_temps']
                                model_current = model_arch['model_current']
                                model_decoupled = model_arch['model_decoupled']

                                y_train = []
                                x_train = []
                                td_errors = []

                                for index_state, state in enumerate(batch_experience):

                                    #y_index = np.argmax(state['q'])
                                    y_val = np.array(state['q'])
                                    y_val[state['action']] = state['r']

                                    if not state['x_1'] is None:

                                        q_values = optimized_x1_dict[index_state]
                                        # Same optimized version as following
                                        # x_input = reshape_to_single_timestep_state(state['x_1'])
                                        # q_values = np.reshape(model_target.predict(x_input, batch_size=1), (dimensions_actions,))

                                        #if not params['is_generate_human_samples']:
                                        #    action, q_values = get_action(q_values)

                                        # Pure Bellman equation with DDQN via target network or pure-DDQN
                                        y_val[state['action']] = state['r'] + params['bellman_gamma'] * np.max(q_values)

                                    state['y_bellman'] = y_val

                                    q_values = optimized_x0_dict[index_state]
                                    # Same optimized version as following
                                    # x_input = reshape_to_single_timestep_state(state['x_0'])
                                    # q_values = np.reshape(model_target.predict(x_input, batch_size=1), (dimensions_actions,))

                                    if params['is_cumulative_reward']:
                                        if params['is_online']:
                                            raise Exception('is_cumulative_reward cannot be used in is_online mode')
                                        else:
                                            y_val[state['action']] = state['r_cumulative']

                                    td_errors.append( np.sum( np.abs(np.array(y_val) - np.array(q_values) )) )

                                    x_train.append(state['x_0'])

                                    if params['architecture'] == 'n_to_n':
                                        y_val = [y_val[idx_arch]]

                                    y_train.append(np.array(y_val))

                                experience.update_priority(batch_rank_e_id, td_errors)

                                if not params['is_memory_unbiased']:
                                    batch_w = None

                                if params['architecture'] == 'n_to_n':
                                    y_train = np.reshape(np.array(y_train), len(y_train), 1)

                                each_loss = model_current.train_on_batch(np.array(x_train), np.array(y_train),
                                                                      sample_weight=batch_w)

                                if math.isnan(each_loss):
                                    # Gradient exploded
                                    loss = each_loss
                                    break
                                else:
                                    losses.append(each_loss)

                                if math.isnan(loss):
                                    break

                        # epoch
                        # if gradient explodes do not wait till end of epoch, store results and quit
                        if (is_game_over and variables['frames_epoch'] >= params['frames_per_epoch']) or math.isnan(loss):

                            experience.rebalance()

                            # Add task id to stop.txt in order to stop execution graciously
                            if os.path.exists('./stop.txt'):
                                stop_tasks = open('./stop.txt', 'r')
                                for line in stop_tasks.readlines():
                                    if line.strip() == 'all' or line.strip() == str(params['id']):
                                        is_force_stop = True
                                        break
                                stop_tasks.close()

                            if not math.isnan(loss):
                                loss = np.average(np.array(losses))

                            variables['epochs'] += 1

                            epoch_reward_max = np.max(variables['epoch_rewards'])
                            epoch_reward_min = np.min(variables['epoch_rewards'])
                            epoch_reward_median = np.median(variables['epoch_rewards'])
                            epoch_reward_avg = np.average(variables['epoch_rewards'])

                            variables['epoch_score_max'] = np.max(variables['epoch_scores'])
                            variables['epoch_score_min'] = np.min(variables['epoch_scores'])
                            variables['epoch_score_avg'] = np.average(variables['epoch_scores'])
                            variables['epoch_score_median'] = np.median(variables['epoch_scores'])

                            epoch_time = round((int(time.time()) - variables['epoch_time_started']) / 60.0, 2)
                            variables['time_total'] += epoch_time

                            curriculum_str = ''
                            if params['is_curriculum_learning']:
                                if params['game'] == 'raycastmaze':
                                    curriculum_str = ';'.join([
                                        utils.float_to_csv_str(variables['curriculum_raycastmaze_size']),
                                        utils.float_to_csv_str(variables['curriculum_raycastmaze_dist'])
                                    ])

                            loss_csv.write(
                                ';'.join([
                                    str(variables['epochs']),
                                    str(variables['episodes_epoch']),
                                    utils.float_to_csv_str(loss),
                                    utils.float_to_csv_str(epoch_reward_max),
                                    utils.float_to_csv_str(epoch_reward_min),
                                    utils.float_to_csv_str(epoch_reward_median),
                                    utils.float_to_csv_str(epoch_reward_avg),
                                    utils.float_to_csv_str(variables['epoch_score_max']),
                                    utils.float_to_csv_str(variables['epoch_score_min']),
                                    utils.float_to_csv_str(variables['epoch_score_median']),
                                    utils.float_to_csv_str(variables['epoch_score_avg']),
                                    curriculum_str,
                                    utils.float_to_csv_str(epoch_time)]) + '\n')

                            logging.info(
                                'epoch: {} episode_per_epoch: {} loss: {} score_max: {} avg: {} curriculum:{} time: {}'.format(
                                    variables['epochs'],
                                    variables['episodes_epoch'],
                                    utils.float_to_csv_str(loss, 3),
                                    utils.float_to_csv_str(variables['epoch_score_max'], 3),
                                    utils.float_to_csv_str(variables['epoch_score_avg'], 3),
                                    curriculum_str,
                                    epoch_time))


                            # Save the best model found so far
                            model_prefixes = ['last']
                            if variables['best_epoch_score_avg'] <= variables['epoch_score_avg']:
                                variables['best_epoch_score_max'] = variables['epoch_score_max']
                                variables['best_epoch_score_min'] = variables['epoch_score_min']
                                variables['best_epoch_score_avg'] = variables['epoch_score_avg']
                                variables['best_epoch_score_median'] = variables['epoch_score_median']
                                variables['best_epoch_loss'] = loss
                                model_prefixes.append('best')

                            for prefix in model_prefixes:
                                for idx_arch, model_arch in enumerate(model_arch_group):
                                    model_target = model_arch['model_target']
                                    model_temps = model_arch['model_temps']

                                    model_target.save('./models/{}_model_target-{}-{}.h5'.format(prefix, idx_arch, params['id']))
                                    for i in range(model_temps_count):
                                        model_temps[i].save('./models/{}_model_temp_{}-{}-{}.h5'.format(prefix, i, idx_arch, params['id']))

                                np.save('./models/{}_variables-{}.npy'.format(prefix, params['id']), variables)
                                if params['is_save_experience']:
                                    experience.save('./models/{}_experience-{}.npy'.format(prefix, params['id']))

                            loss_csv.flush()

                            if not math.isnan(loss):
                                # detect blowup
                                if params['is_blowup_detection'] and len(variables['all_epoch_loss']) > 0:
                                    loss_prev = variables['all_epoch_loss'][-1]

                                    loss_power_prev = math.floor(math.log10(math.fabs(loss_prev)))
                                    loss_power = math.floor(math.log10(math.fabs(loss)))

                                    if len(variables['all_epoch_avg_scores']) >= params['blowup_score_const_epochs']:
                                        epoch_scores_test = variables['all_epoch_avg_scores'][-params['blowup_score_const_epochs']:]
                                        is_same_score = True
                                        for each_test in epoch_scores_test:
                                            if each_test != variables['epoch_score_avg']:
                                                is_same_score = False
                                                break
                                        if is_same_score:
                                            variables['is_blownup'] = True
                                            logging.info('is_blownup: same score')

                                        if not variables['is_blownup']:
                                            is_worse_score = True
                                            for each_test in epoch_scores_test:
                                                if each_test > variables['epoch_score_avg']:
                                                    is_worse_score = False
                                                    break

                                            if is_worse_score:
                                                if (loss_power - loss_power_prev >= params['blowup_loss_plus_order']):
                                                    logging.info('is_blownup: loss plus, score worse')
                                                    variables['is_blownup'] = True

                                                if len(variables['all_epoch_avg_scores']) >= params['blowup_loss_minus_epochs']:
                                                    if (loss_power - loss_power_prev >= params['blowup_loss_minus_order']):
                                                        logging.info('is_blownup: loss minus, score worse')
                                                        variables['is_blownup'] = True


                                variables['all_epoch_loss'].append(loss)


                            variables['all_epoch_avg_scores'].append(variables['epoch_score_avg'])
                            variables['all_epoch_max_scores'].append(variables['epoch_score_max'])
                            variables['all_epoch_min_scores'].append(variables['epoch_score_min'])
                            variables['all_epoch_median_scores'].append(variables['epoch_score_median'])

                            variables['epoch_is_qmap_generated'] = False
                            variables['frames_epoch'] = 0
                            variables['episodes_epoch'] = 0
                            variables['epoch_scores'] = []
                            variables['epoch_rewards'] = []
                            variables['epoch_time_started'] = int(time.time())

                            losses = []

                            if math.isnan(loss) or variables['is_blownup']:
                                # learning exploded
                                break

                        # update target model with time interval / threshold of episodes
                        if params['target_network_alpha'] > 0.0 and variables['frames_target_network'] >= params['frames_update_target_network']:
                            variables['frames_target_network'] = 0

                            for idx_arch, model_arch in enumerate(model_arch_group):
                                model_target = model_arch['model_target']
                                model_temps = model_arch['model_temps']
                                model_current = model_arch['model_current']
                                model_decoupled = model_arch['model_decoupled']

                                if params['target_network_alpha'] == 1.0:
                                    # completely replace target model weight like most of DQN papers suggest
                                    model_target.set_weights(model_current.get_weights())
                                else:
                                    # apply weights with alpha to target network
                                    model_target.set_weights((np.array(model_target.get_weights()) * (1.0 - params['target_network_alpha']) + np.array(model_current.get_weights()) * params['target_network_alpha']).tolist())
                                    model_current.set_weights(model_target.get_weights())

                                #TODO research direction - dynamically change alpha depending on relative loss, hiher loss smaller alpha

            # TODO FPS adjust to real
            if not params['is_save_qmap']:
                if params['is_playback'] or params['is_generate_human_samples']:
                    time.sleep(0.03)
                    #time.sleep(0.3)

            if is_game_over:
                break

            prev_state_orig = np.array(state_orig)
            prev_state = np.array(state)
            if not action is None:
                prev_action = action


        if is_force_stop:
            break
        if params['is_playback']:
            logging.info('game over')

        if params['is_curriculum_learning']:
            percentage_of_learning =  (variables['frames_total'] / float(params['frames_per_epoch'] * params['nb_epochs_end_of_learning']))
            if params['game'] == 'raycastmaze':
                variables['curriculum_raycastmaze_size'] = int(params['curriculum_raycastmaze_size_start'] + percentage_of_learning * float(params['curriculum_raycastmaze_size_end'] - params['curriculum_raycastmaze_size_start']))
                variables['curriculum_raycastmaze_dist'] = int(params['curriculum_raycastmaze_dist_start'] + percentage_of_learning * float(params['curriculum_raycastmaze_dist_end'] - params['curriculum_raycastmaze_dist_start']))
                env.game.map_size = variables['curriculum_raycastmaze_size']
                env.game.init_pos_distance_to_target = variables['curriculum_raycastmaze_dist']

        env.reset_game()

        variables['episodes_total'] += 1
        variables['episodes_epoch'] += 1

        # Decaying epsilon for e-greedy
        if variables['frames_total'] > params['frames_before_start_training']:
            if params['is_e_greedy']:
                variables['epsilon_explore'] -= epsilon_explore_decay_per_episode
                variables['epsilon_explore'] = max(variables['epsilon_explore'], params['epsilon_explore_end'])
            if params['is_e_stuck']:
                variables['epsilon_e_stuck'] -= epsilon_stuck_decay_per_episode
                variables['epsilon_e_stuck'] = max(variables['epsilon_e_stuck'], params['e_stuck_explore_end'])

main()

if not params['is_generate_human_samples'] and not params['is_playback']:

    # After training has been completed
    str_db = './results/{}.csv'.format(params['results_db'])
    is_existing_db = os.path.exists(str_db)

    headers = None
    if is_existing_db:
        results_db_csv = open(str_db, 'r')
        utils.lock_file(results_db_csv)
        header_line = results_db_csv.readline().strip()
        headers = header_line.split(';')
        utils.unlock_file(results_db_csv)
        results_db_csv.close()

    results_db_csv = open(str_db, 'a')
    # avoid overwriting of file
    utils.lock_file(results_db_csv)

    eval_max = np.max(np.array(variables['all_epoch_max_scores'][-min(params['evaluation_stride'], len(
        variables['all_epoch_max_scores'])):]))
    eval_avg = np.average(np.array(variables['all_epoch_avg_scores'][-min(params['evaluation_stride'], len(
        variables['all_epoch_avg_scores'])):]))
    eval_median = np.median(np.array(variables['all_epoch_median_scores'][-min(params['evaluation_stride'], len(
        variables['all_epoch_median_scores'])):]))
    eval_min = np.min(np.array(variables['all_epoch_min_scores'][-min(params['evaluation_stride'], len(
        variables['all_epoch_min_scores'])):]))

    if math.isnan(loss):
        eval_loss = -1
    else:
        eval_loss = np.average(np.array(variables['all_epoch_loss'][-min(params['evaluation_stride'], len(
            variables['all_epoch_loss'])):]))

    first_columns = [
                'id',
                'best_avg',
                'best_min',
                'best_max',
                'best_median',
                'best_loss',

                'end_avg',
                'end_min',
                'end_max',
                'end_median',
                'end_loss',

                'eval_avg',
                'eval_min',
                'eval_max',
                'eval_median',
                'eval_loss',

                'time_total',
                'time_avg',
            ]


    if not is_existing_db:
        results_db_csv.write(
            ';'.join(first_columns))

        results_db_csv.write(';')
        for key in params.keys():
            results_db_csv.write(key + ';')
        results_db_csv.write('params_argv')
        results_db_csv.write('\n')

        headers = params.keys() + ['params_argv']
    else:
        headers = headers[len(first_columns):]

    results_db_csv.write(str(params['id']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['best_epoch_score_avg']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['best_epoch_score_min']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['best_epoch_score_max']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['best_epoch_score_median']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['best_epoch_loss']) + ';')

    results_db_csv.write(utils.float_to_csv_str(variables['epoch_score_avg']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['epoch_score_min']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['epoch_score_max']) + ';')
    results_db_csv.write(utils.float_to_csv_str(variables['epoch_score_median']) + ';')

    if len(variables['all_epoch_loss']) == 0:
        variables['all_epoch_loss'] = ['nan']

    results_db_csv.write(utils.float_to_csv_str(variables['all_epoch_loss'][-1]) + ';')

    results_db_csv.write(utils.float_to_csv_str(eval_avg) + ';')
    results_db_csv.write(utils.float_to_csv_str(eval_min) + ';')
    results_db_csv.write(utils.float_to_csv_str(eval_max) + ';')
    results_db_csv.write(utils.float_to_csv_str(eval_median) + ';')
    results_db_csv.write(utils.float_to_csv_str(eval_loss) + ';')

    results_db_csv.write(utils.float_to_csv_str(variables['time_total']) + ';')

    if(variables['epochs'] == 0):
        variables['epochs'] = 1
    results_db_csv.write(utils.float_to_csv_str(round(variables['time_total'] / variables['epochs'], 2)) + ';')

    params['params_argv'] = param_utils.format_params(params_argv);
    for key in headers:
        if not key in params:
            results_db_csv.write('?;')
        else:
            results_db_csv.write(utils.float_to_csv_str(params[key]) + ';')
    results_db_csv.write('\n')

    results_db_csv.flush()

    utils.unlock_file(results_db_csv)
    results_db_csv.close()

params_csv.close()
loss_csv.close()