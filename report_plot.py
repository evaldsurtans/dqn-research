import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import colorlover as cl #sudo pip install colorlover
import os

import numpy as np
import re
import sys
import pprint
import numbers
import traceback
from os.path import basename
import argparse

parser = argparse.ArgumentParser(description='Plotly visualizations')


parser.add_argument('--type', help='Chart type: loss, histogram', default='loss', type=str)
parser.add_argument('--label', help='Series labels: comment, lr, etc', default='comment', type=str)

parser.add_argument('--include', help='Include repeat_id, example: 1 2 3', nargs='*')

parser.add_argument('--plotly_username', help='Plotly username', default=os.environ['PLOTLY_USERNAME'], type=str)
parser.add_argument('--plotly_apikey', help='Plotly apikey', default=os.environ['PLOTLY_APIKEY'], type=str)

parser.add_argument('input', help='Path to results CSV', default='', type=str)

args = parser.parse_args()

plotly.tools.set_credentials_file(username=args.plotly_username, api_key=args.plotly_apikey)

input_path = args.input

input_param = args.type
# histogram - variance by repeat_id
# loss - loss over epochs by repeat_id

input_label = args.label
#input_label = 'lr'

input_param_histogram = 'eval_avg'
input_param_loss = 'epoch_score_avg'

input_skip_repeat_id = []
input_include_repeat_id = []
input_include_labels = []

if args.include:
    input_include_repeat_id += [int(it) for it in args.include]

input_dir = input_path[:input_path.rfind('/') + 1]

# parse main CSV summary file
def read_csv(filename):
    input_lines = open(filename).readlines()
    header = None
    rows = []
    for line in input_lines:
        row = line.strip().split(';')
        # last empty element
        if len(row[-1].strip()) == 0:
            row = row[:-1]

        if header is None:
            header = row
        elif len(row) == len(header):
            formated_row = {}
            for idx, each in enumerate(row):
                if(idx < len(header)):
                    if re.match('^[0-9E\+\-\.]+$', each):
                        formated_row[header[idx]] = float(each)
                    else:
                        formated_row[header[idx]] = each
            rows.append(formated_row)
    return rows

rows = read_csv(input_path)
plot_yaxis_min = float('Inf')
plot_yaxis_max = float('-Inf')

# group by repeat_id
groups = []
group = {
    'repeat_id': rows[0]['repeat_id'],
    'rows': []
}
groups.append(group)
for row in rows:
    group = filter(lambda it: it['repeat_id'] == row['repeat_id'], groups)
    if len(group) > 0:
        group = group[0]
    else:
        group = {
            'repeat_id': row['repeat_id'],
            'rows': []
        }
        groups.append(group)
    group['rows'].append(row)


def get_data_loss(rows, color, label):
    global plot_yaxis_max, plot_yaxis_min

    comment = str(rows[0][input_label])
    if not label is None:
        comment = label
    x = []
    y = []
    y_upper = []
    y_lower = []
    y_avg = []

    for row in rows:
        try:
            loss_rows = read_csv(input_dir + 'loss-' + str(int(row['id'])) + '.csv')
            for idx, loss_row in enumerate(loss_rows):
                if len(y) <= idx:
                    y.append([])
                val = loss_row[input_param_loss]
                if isinstance(val, numbers.Number):
                    y[idx].append(val)
                else:
                    pass
        except:
            pass

    for idx, each in enumerate(y):
        try:
            x.append(idx+1)
            y_avg.append(np.mean(each))
            y_upper.append(np.max(each))
            y_lower.append(np.min(each))
            plot_yaxis_max = max(plot_yaxis_max, np.max(each))
            plot_yaxis_min = min(plot_yaxis_min, np.min(each))
        except:
            traceback.print_exc()
            pprint.pprint(idx)
            pprint.pprint(each)
            print comment
            exit()

    x_rev = x[::-1]
    y_lower = y_lower[::-1]

    data = []
    data.append(go.Scatter(
        x=x + x_rev,
        y=y_upper + y_lower,
        fill='tozerox',
        fillcolor='rgba({},{},{},0.2)'.format(color[0], color[1], color[2]),
        line= go.Line(color='transparent'),
        showlegend=False,
        name=comment,
    ))

    data.append(go.Scatter(
        x=x,
        y=y_avg,
        line=go.Line(color='rgb({},{},{})'.format(color[0], color[1], color[2])),
        mode='lines',
        showlegend=True,
        name=comment,
    ))

    return data

colors = cl.to_numeric( cl.scales['3']['qual']['Set1'] )
count_colors = len(groups)
if len(input_include_repeat_id) > 0:
    count_colors = len(input_include_repeat_id)
if(count_colors > 3):
    colors = cl.to_numeric( cl.scales[str(count_colors)]['qual']['Paired'] )
data = []
c_idx = 0
for idx, group in enumerate(groups):
    if group['repeat_id'] == '' or int(group['repeat_id']) in input_skip_repeat_id:
        continue

    if len(input_include_repeat_id) > 0 and not int(group['repeat_id']) in input_include_repeat_id:
        continue

    if input_param == 'histogram':
        x = [row[input_param_histogram] for row in group['rows']]
        data.append(go.Histogram(x=x,
                                 opacity=0.75,
                                 #xbins=go.XBins(start=min(x),
                                 #            end=max(x),
                                 #            size=20),
                                 name=str(group['rows'][0][input_label])) )
    elif input_param == 'loss':
        label = None
        if len(input_include_labels) > 0:
            label = input_include_labels[input_include_repeat_id.index(int(group['repeat_id']))]
        data += get_data_loss(group['rows'], colors[c_idx], label)
        c_idx += 1
        if c_idx > count_colors:
            c_idx = 0

yaxis = None
if(input_param == 'loss'):
    yaxis = dict(autorange=False, range=[plot_yaxis_min, plot_yaxis_max])

layout = go.Layout(barmode='overlay', yaxis=yaxis)
fig = go.Figure(data=data, layout=layout)
py.plot(fig, filename=basename(input_path) + '_' + input_param + '_' + input_param_histogram)
