from __future__ import print_function
import numpy as np
import math
import random
import sys

print('Number of arguments: %d arguments' % len(sys.argv))
print('Argument List: %s' % str(sys.argv))

np.random.seed(None)
max_seed =  2**32 - 1

rand_seed = int(sys.argv[1]) if len(sys.argv) > 1 else 0

if rand_seed:
    np.random.seed(rand_seed)
else:
    seed = np.random.randint(max_seed)
    np.random.seed(seed)
    rand_seed = seed

print("rand_seed=%d"%rand_seed)
np.random.seed(rand_seed)

# following based on numpy seed
import tensorflow as tf
tf.set_random_seed(np.random.randint(max_seed))
random.seed(np.random.randint(max_seed)) # for experience replay


for i in range(20):
    print(random.randint(1,1000))
print("")
for i in range(20):
    print(np.random.randint(1000))
