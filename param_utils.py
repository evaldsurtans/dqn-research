import sys
import json
import re
import logging

def format_params(params):
    list_txt = []
    for key in params.keys():
        value = params[key]
        if isinstance(value, basestring):
            value = value.replace(' ', '_')
            value = value.replace(',', '_')
            value = value.replace('__', '_')
            value = '^' + value + '^'
        list_txt.append('^' + key + '^:' + str(value).lower())
    return ('*').join(list_txt)


def load_params(original_params):
    params = {}
    params_string = ''

    if len(sys.argv) > 1:
        params_string = sys.argv[1]
        print params_string
        temp = params_string.replace("^", '"')
        temp = str( '{' + temp.replace('*', ',') + '}' )
        params = json.loads( temp )

    for key in original_params.keys():
        if key in params.keys():
            original_params[key] = params[key]

    return original_params, params

def init_log(filename):
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.level = logging.DEBUG

    fileHandler = logging.FileHandler(filename)
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)